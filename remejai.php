<?php
$title = "Rėmėjai";
include "header.php"; ?>
<div class="wrapper">
  <div class="main">
    <h1>Rėmėjai</h1>
    <div class="remejai col-12">
    <div class="row">
        <h2>Generalinis rėmėjas</h2>
        <div class="col-12">
            <img class="a" id="genr" src="img/remejai/sumeda.png" alt="Sumeda">
        </div>
    </div>
    <div class="row">
        <h2>Rėmėjai</h2>
        <div class="col-12">
            <div class="col-4 col-m-4 col-md-3 col-s-6"><img src="img/remejai/sr.png" alt="Statybos ritmas"></div>
            <div class="col-4 col-m-4 col-md-3 col-s-6"><img src="img/remejai/arvi.png" alt="Arvi"></div>
            <div class="col-4 col-m-4 col-md-3 col-s-6"><img src="img/remejai/herbas.png" alt="Marijampolės savivaldybė"></div>
            <br class="clear">
        </div>
    </div>
    <div class="row">
        <h2>Kiti rėmėjai</h2>
        <div class="col-12">
            <div class="col-2  col-md-3 col-s-6"><img src="img/remejai/garfus.png" alt="Garfus"></div>
            <div class="col-2  col-md-3 col-s-6"><img src="img/remejai/mavista.png" alt="Mavista"></div>
            <div class="col-2  col-md-3 col-s-6"><img src="img/remejai/joma.png" alt="Joma"></div>
            <div class="col-2  col-md-3 col-s-6"><img src="img/remejai/kttape.png" alt="Kttape"></div>
            <div class="col-2  col-md-3 col-s-6"><img src="img/remejai/iceco.png" alt="Iceco"></div>
            <div class="col-2  col-md-3 col-s-6"><img src="img/remejai/laikr.png" alt="Piko valanda"></div>
        </div>
    </div>
    <div class="row">
        <h2>Informaciniai rėmėjai</h2>
        <div class="col-12">
            <div class="col-3  col-md-3 col-s-6"><img src="img/remejai/marijampolestv.png" alt="Marijampolės televizija"></div>
            <div class="col-3  col-md-3 col-s-6"><img src="img/remejai/100proc.png" alt="100 procentų"></div>
            <div class="col-3  col-md-3 col-s-6"><img src="img/remejai/kapsai.png" alt="Kapsai"></div>
            <div class="col-3  col-md-3 col-s-6"><img src="img/remejai/suvalkietis.png" alt="Suvalkietis"></div>
        </div>
    </div>
</div>
  </div>
</div>
<?php include "footer.php"; ?>
