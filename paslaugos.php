<?php
include "connectDB.php";
$title = "Paslaugos";
 include "header.php"; ?>
<div class="wrapper">
  <div class="main">
    <h1>Paslaugos</h1>
    <div class="col-12 baltas">
      <h2>Mūsų teikiamos paslaugos</h2>
      <p>Futbolas Marijampolės mieste jau daugelį metų užima svarbią vietą, o pagal stadiono lankomumą Marijampolė pirmauja kitų miestų atžvilgiu. Pastačius daugiafunkcinį sporto ir turizmo kompleksą, atsirado galimybė vystyti futbolo sporto šaką ištisus metus, todėl čia visuomet yra laukiami tiek Marijampolės miesto sportininkai ir futbolo mėgėjai, tiek kitų Lietuvos miestų svečiai bei svečiai iš užsienio. Sporto kompleksas yra puiki vieta organizuoti trumpalaikes ar ilgalaikes futbolo stovyklas, futbolo turnyrus. Atvykstantiems galime pasiūlyti puikias treniravimosi sąlygas futbolo aikštyne ar manieže, suteikiant visą reikiamą inventorių, transporto, skalbyklos, saunos ir baseino paslaugas, apgyvendinimą netoli sporto komplekso, maitinimą. <br>
Siekdami suteikti savo lankytojams kuo daugiau malonių akimirkų ir kuo mažiau rūpesčių, bei norėdami kiekvieną apsilankymą mūsų sporto komplekse padaryti įsimintinu, futbolo komandoms iš Lietuvos ir užsienio siūlome bendrą paslaugų paketą, kurį sudaro:
Treniruotės futbolo aikštyne
Futbolo treniruotės manieže
Apgyvendinimo ir maitinimo paslaugos
Transporto paslaugos
Baseinas, sauna
Skalbyklos paslauga
*Maniežas gali būti nuomojamas ne tik sporto, bet ir masiniams kultūriniams renginiams organizuoti (koncertams, mugėms, parodoms, kt.)
Sporto kompleksas laukia ne tik futbolo profesionalų, bet ir futbolo mėgėjų, norinčių savo gabumus išmėginti futbolo aikštėje. Jūsų patogumui galime pasiūlyti išsinuomoti visą, pusę ar ketvirtadalį futbolo aikštės manieže draugiškoms varžyboms, šeimos turnyrams surengti.
Sukate galvą, kokią dovaną gimtadienio proga įteikti draugui? Nežinote, kur smagiai ir aktyviai praleisti laisvalaikį su draugais ar šeima?  Atvykite pas mus ir kartu rasime geriausią sprendimą! Gera nuotaika ir smagus laikas su mielais žmonėmis garantuotas!
Maksimaliai geros emocijos ir įsimintini įspūdžiai už prieinamą kainą!
<br><br>
Dėl papildomos informacijos ir užsakymų prašome kreiptis: <br>
Tel. (8~ 343) 72325 <br>
Faks (8~ 343) 33155 <br>
Mob. 8 687 41286 <br>
El. paštas: fkclub@gmail.com</p>
    </div>
  </div>
</div>
<br class="clear">
<?php include "footer.php"; ?>
