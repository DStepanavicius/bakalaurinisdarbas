<?php
include "connectDB.php";
$title = "Duomenų keitimas";
include "header.php"; ?>
  <div class="main wrapper">
    <section style="margin-top: 25px;" class="naujienosKita">
      <div class="naujWrapper">
        <?php
        if (isset($_SESSION['id'])) {
          if (strpos($_SERVER['REQUEST_URI'], '?naujienos')) {
          
            $id = $_POST['id'];
            $image = $_POST['image'];
            $head = $_POST['head'];
            $content = $_POST['content'];
            $naujSkiltis = $_POST['naujSkiltis'];
            $turas = $_POST['turas'];
            $rusis = $_POST['rusis'];

            echo "
            <form action='".tvarkymasNaujienos($conn)."' method='post'>
            <table id='alyga'>
            <tr>
            <th>Nuotrauka</th>
            <th>Antraštė</th>
           
            <th>Skiltis</th>
            <th>Turas</th>
            <th>Rūšis</th>
            </tr>
            <tr>
              <td><input  name='id' type='hidden' value='" . $id . "'><input  name='image' type='text' value='" . $image . "'></td>
              <td><input  name='head' type='text' value='" . $head . "'></td>
              
              <td><input  name='naujSkiltis' type='text' value='" . $naujSkiltis . "'></td>
              <td><input  name='turas' type='text' value='" . $turas . "'></td>
              <td><input  name='rusis' type='text' value='" . $rusis . "'></td>
            </tr>
            <tr>
              <th colspan='6'>Turinys</th>
            </tr>
            <tr>
              <td colspan='6'><textarea style='height:350px;'  name='content' type='text'>". $content ."</textarea></td>
            </tr>
              </table>
              <input class='keitimas' name='keisti' type='submit' value='Keisti'>
              <input class='keitimas' name='trinti' type='submit' value='Trinti'>
            </form>
            ";
          } elseif (strpos($_SERVER['REQUEST_URI'], '?nuotraukos')) {
            $id = $_POST['id'];
            $failas = $_POST['failas'];
            $skiltis = $_POST['skiltis'];
            $lyga = $_POST['lyga'];
            $turas = $_POST['turas'];

            echo "
            <form action='".tvarkymasNuotraukos($conn)."' method='post'>
            <table id='alyga'>
            <tr>
              <th>Failas</th>
              <th>Skiltis</th>
              <th>Lyga</th>
              <th>Turas</th>
            </tr>
            <tr>
              <td><input  name='id' type='hidden' value='" . $id . "'><input  name='failas' type='text' value='" . $failas . "'></td>
              <td><input  name='skiltis' type='text' value='" . $skiltis . "'></td>
              <td><input  name='lyga' type='text' value='" . $lyga . "'></td>
              <td><input  name='turas' type='text' value='" . $turas . "'></td>
            </tr>
              </table>
              <input class='keitimas' name='keisti' type='submit' value='Keisti'>
              <input class='keitimas' name='trinti' type='submit' value='Trinti'>
            </form>";
          } elseif (strpos($_SERVER['REQUEST_URI'], '?video')) {
            $id = $_POST['id'];
            $failas = $_POST['failas'];
            $antraste = $_POST['antraste'];
            $lyga = $_POST['lyga'];
            echo "
            <form action='".tvarkymasMedia($conn)."' method='post'>
            <table id='alyga'>
            <tr>
              <th>Failas</th>
              <th>Antraštė</th>
              <th>Lyga</th>
            </tr>
            <tr>
              <td><input  name='id' type='hidden' value='" . $id . "'><input  name='failas' type='text' value='" . $failas . "'></td>
              <td><input  name='antraste' type='text' value='" . $antraste . "'></td>
              <td><input  name='lyga' type='text' value='" . $lyga . "'></td>

            </tr>
              </table>
              <input class='keitimas' name='keisti' type='submit' value='Keisti'>
              <input class='keitimas' name='trinti' type='submit' value='Trinti'>
            </form>";
          } elseif (strpos($_SERVER['REQUEST_URI'], '?lentele')) {
            $skomanda = $_POST['skomanda'];
            $taskai = $_POST['taskai'];
            $suzaista = $_POST['suzaista'];
            $komanda = $_POST['komanda'];
            $komlogo = $_POST['logo'];

            echo "
            <form action='".tvarkymasLenteles($conn)."' method='post'>
            <table id='alyga'>
            <tr>
              <th>Komanda</th>
              <th>Sužaidė</th>
              <th>Taškai</th>
              <th>Logo</th>
            </tr>
            <tr>
              <td><input  name='skomanda' type='hidden' value='" . $skomanda . "'><input  name='komanda' type='text' value='" . $komanda . "'></td>
              <td><input  name='suzaista' type='text' value='" . $suzaista . "'></td>
              <td><input  name='taskai' type='text' value='" . $taskai . "'></td>
              <td><input  name='logo' type='text' value='" . $komlogo . "'></td>
            </tr>
              </table>
              <input class='keitimas' name='keisti' type='submit' value='Keisti'>
              <input class='keitimas' name='trinti' type='submit' value='Trinti'>
            </form>";
          } elseif (strpos($_SERVER['REQUEST_URI'], '?rungtynes')) {
            $id = $_POST['id'];
            $turas = $_POST['turas'];
            $sturas = $_POST['sturas'];
            $data = $_POST['data'];
            $komanda1 = $_POST['komanda1'];
            $komanda2 = $_POST['komanda2'];
            $ivarciai1 = $_POST['ivarciai1'];
            $ivarciai2 = $_POST['ivarciai2'];
            $irasas = $_POST['irasas'];
            $logo = $_POST['logo'];
            $lyga = $_POST['lyga'];
            echo "
            <form action='".tvarkymas($conn)."' method='post'>
            <table id='alyga'>
            <tr>
              <th>Nr.</th>
              <th>Komanda 1</th>
              <th>Komanda 2</th>
              <th>Data</th>
              <th>Įvartis 1</th>
              <th>Įvartis 2</th>
              <th>Įrašas</th>
              <th>Logo</th>
              <th>Lyga</th>

            </tr>
            <tr>
            <td><input  name='id' type='hidden' value='" . $id . "'></td>
              <td><input  name='sturas' type='hidden' value='" . $sturas . "'><input  name='turas' type='text' value='" . $turas . "'></td>
              <td><input  name='komanda1' type='text' value='" . $komanda1 . "'></td>
              <td><input  name='komanda2' type='text' value='" . $komanda2 . "'></td>
              <td><input  name='data' type='text' value='" . $data . "'></td>
              <td><input  name='ivarciai1' type='text' value='" . $ivarciai1."'></td>
              <td><input  name='ivarciai2' type='text' value='" . $ivarciai2. "'></td>
              <td><input  name='irasas' type='text' value='" . $irasas."'></td>
              <td><input  name='logo' type='text' value='" . $logo. "'></td>
              <td><input  name='lyga' type='text' value='" . $lyga. "'></td>

            </tr>

              </table>
              <input class='keitimas' name='keisti' type='submit' value='Keisti'>
              <input class='keitimas' name='trinti' type='submit' value='Trinti'>
            </form>";
          } elseif (strpos($_SERVER['REQUEST_URI'], '?komanda')) {
            $id = $_POST['id'];
            $image = $_POST['image'];
            $name = $_POST['name'];
            $position = $_POST['position'];
            $lenght = $_POST['lenght'];
            $weight = $_POST['weight'];
            $date = $_POST['date'];
            $nation = $_POST['nation'];
            $since = $_POST['since'];
            $goals = $_POST['goals'];
            $ast = $_POST['ast'];
            $saves = $_POST['saves'];

            echo "
            <form action='".tvarkymasKomanda($conn)."' method='post'>
            <table id='alyga'>
            <tr>
              <th>Nuotrauka</th>
              <th>Vardas</th>
              <th>Pozicija</th>
              <th>Ūgis</th>
              <th>Svoris</th>
              <th>Gimimo data</th>
              <th>Tautybė</th>
              <th>Nuo</th>
              <th>Įvarčiai</th>
              <th>Perdavimai</th>
              <th>Išsaugojimai</th>
            </tr>
            <tr>
              <td><input  name='id' type='hidden' value='" . $id . "'><input  name='image' type='text' value='" . $image . "'></td>
              <td><input  name='name' type='text' value='" . $name . "'></td>
              <td><input  name='position' type='text' value='" . $position . "'></td>
              <td><input  name='lenght' type='text' value='" . $lenght . "'></td>
              <td><input  name='weight' type='text' value='" . $weight . "'></td>
              <td><input  name='date' type='text' value='" . $date . "'></td>
              <td><input  name='nation' type='text' value='" . $nation . "'></td>
              <td><input  name='since' type='text' value='" . $since . "'></td>
              <td><input  name='goals' type='text' value='" . $goals . "'></td>
              <td><input  name='ast' type='text' value='" . $ast . "'></td>
              <td><input  name='saves' type='text' value='" . $saves . "'></td>
            </tr>
              </table>
              <input class='keitimas' name='keisti' type='submit' value='Keisti'>
              <input class='keitimas' name='trinti' type='submit' value='Trinti'>
            </form>";
        } else {
          header('Location: admin-panel');
        }
      }
        ?>
         <br class="clear">
        <div id="adminNuorodos">
          <a style="background-color: #0d0d0d" href="admin<?php echo substr($_SERVER['REQUEST_URI'], strpos($_SERVER['REQUEST_URI'], "?"))?>">Grįžti</a>
          <a style="background-color: #0d0d0d" href="logout">Atsijungti</a>
        </div>
      </div>
    </section>
    <br class="clear">
  </div>
<?php include "footer.php"; ?>
