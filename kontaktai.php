<?php
$title = "Kontaktai";
 include "header.php"; ?>
<div class="wrapper">
  <div class="kontaktai main">
    <h1>Kontaktai</h1>
    <div class="kontaktai-info">
      <div class="col-6 col-m-12">
        <ul>
          <li><span>Adresas:</span>Kauno g. 125 Marijampolė</li>
          <li><span>Kontaktinis telefonas:</span>+370 343 71178</li>
          <li><span>Tel/fax:</span>(343) 71178</li>
          <li><br></li>
          <li><span>Klubo prezidentas:</span>Vidmantas Murauskas</li>
          <li><span>Administracijos direktorius:</span>Karolis Skinkys </li>
          <li><span>Mob. Tel.:</span>+37069395702</li>
          <li><span>El. paštas:</span>fksuduva@takas.lt</li>
          <li></li>
          <li><span>Viešųjų ryšių atstovas:</span>Antanas Popiera </li>
          <li><span>Mob. Tel.:</span>+37065819240</li>
          <li><span>El. paštas:</span>antanaera@gmail.com</li>
        </ul>
      </div>
      <div class="col-3 col-m-6 col-md-12">
        <img src="http://fksuduva.lt/wp-content/uploads/2015/01/10942803_339967952858197_1663898744_n-250x268.jpg" alt="">
        <p>Vidmantas Murauskas</p>
      </div>
      <div class="col-3 col-m-6 col-md-12">
        <img src="http://fksuduva.lt/wp-content/uploads/2015/01/Skinkys-250x268.png" alt="">
        <p>Antanas Popiera</p>
      </div>
      <br class="clear">
    </div>
    <h2>Mes randamės</h2>
    <div class="forma col-12">
      <div class="col-12 col-m-12">
        <iframe style="width:100%; height:281px; border:0;" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d18500.982090819158!2d23.369327!3d54.5754!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46e12a41a2ef0fc1%3A0xd4abf34336054cb6!2sKauno+g.+125%2C+Marijampol%C4%97+68226%2C+Lietuva!5e0!3m2!1slt!2sus!4v1495400051202"allowfullscreen></iframe>
      </div>
    </div>
    <br class="clear">
  </div>
</div>
<?php include "footer.php"; ?>
