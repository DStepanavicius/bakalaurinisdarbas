<?php
include "connectDB.php";
$title = "Pagrindinė komanda";
 include "header.php"; ?>
  <div class="wrapper">
    <section style="margin-top: 25px;" class="naujienos">
      <div class="col-12">
        <?php getBestPlayers($conn) ?>
      </div>
      <br class="clear">
      <h2>Pagrindinės komandos sudėtis</h2>
      <div class="zaidejai">
        <h2>Vartininkai</h2>
          <?php getPlayers($conn, "Vartininkas") ?>
          <br class="clear">
        <h2>Gynėjai</h2>
          <?php getPlayers($conn, "Gynėjas") ?>
          <br class="clear">
        <h2>Saugai</h2>
          <?php getPlayers($conn, "Saugas") ?>
          <br class="clear">
        <h2>Puolėjai</h2>
          <?php getPlayers($conn, "Puolėjas") ?>
          <br class="clear">
        <h2>Treneriai</h2>
          <?php getPlayers($conn, "") ?>
          <br class="clear">
      </div>
    </section>
    <br class="clear">
  </div>
<?php include "footer.php"; ?>
