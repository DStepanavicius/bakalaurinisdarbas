<?php
include "connectDB.php";
$title = "Archyvas";
include "header.php"; ?>
  <div class="wrapper">
    <section class="naujienos naujienosKita">
      <h2>Archyvas</h2>
      <div class="naujWrapper">
        <?php paginate($conn, "naujienos", "Archyvas") ?>
      </div>
    </section>
    <br class="clear">
  </div>
<?php include "footer.php"; ?>
