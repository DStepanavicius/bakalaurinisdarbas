<?php
include "connectDB.php";
$title = "Nuotraukos";
 include "header.php"; ?>
<div class="wrapper">
  <div class="main">

      <?php
        if (strpos(htmlspecialchars($_SERVER['REQUEST_URI']), '?a-lyga')) {
          echo '
          <h1>A lygos nuotraukos</h1>
          <div class="col-12">
            <a href="nuotraukos" class="filter_link">Visi</a>
            <a style="background-color: #0d0d0d;" href="nuotraukos?a-lyga" class="filter_link">A lyga</a>
            <a href="nuotraukos?II-lyga" class="filter_link">II lyga</a>
          </div>
          <div class="col-12">
          ';
          paginateMedia($conn, "A lyga", "paveikslėliai", 12);
        } elseif (strpos(htmlspecialchars($_SERVER['REQUEST_URI']), '?II-lyga')) {
          echo '
          <h1>II lygos nuotraukos</h1>
          <div class="col-12">
            <a href="nuotraukos" class="filter_link">Visi</a>
            <a href="nuotraukos?a-lyga" class="filter_link">A lyga</a>
            <a style="background-color: #0d0d0d;" href="nuotraukos?II-lyga" class="filter_link">II lyga</a>
          </div>
          <div class="col-12">
          ';
          paginateMedia($conn, "II lyga", "paveikslėliai", 12);
        } elseif (strpos(htmlspecialchars($_SERVER['REQUEST_URI']), '?turas')) {
          echo '
          <h1>'.substr($_SERVER['REQUEST_URI'], strpos(htmlspecialchars($_SERVER['REQUEST_URI']), "=")+1).' turo nuotraukos</h1>
          <div class="col-12">
          ';

          paginatetest($conn, substr(htmlspecialchars($_SERVER['REQUEST_URI']), strpos(htmlspecialchars($_SERVER['REQUEST_URI']), "=")+1), "paveikslėliai");
        } else {
          echo '
          <h1>Nuotraukos</h1>
          <div class="col-12">
            <a href="nuotraukos" style="background-color: #0d0d0d;" class="filter_link">Visi</a>
            <a href="nuotraukos?a-lyga" class="filter_link">A lyga</a>
            <a href="nuotraukos?II-lyga" class="filter_link">II lyga</a>
          </div>
          <div class="col-12">
          ';
          paginateMedia($conn, "visos", "paveikslėliai", 12);
        }
       ?>
  </div>
  <br class="clear">
  </div>
</div>
<?php include "footer.php"; ?>
