<?php
include "connectDB.php";
$title = "Svarbi informacija";
 include "header.php"; ?>
<div class="wrapper">
  <div class="informacija main">
    <h1>Svarbi informacija</h1>
    <div class="col-12 baltas">
      <h2>Pagrindinės nuostatos</h2>
      <p>Siekdami abipusio bendradarbiavimo ir norėdami užtikrinti Marijampolės sporto komplekso lankytojų  saugumą, kviečiame susipažinti su stadiono ir sporto maniežo lankytojų vidaus elgesio taisyklėmis. Atkreipiame dėmesį, kad šių taisyklių nežinojimas, neatleidžia taisyklėse nustatytų reikalavimų nesilaikančių asmenų nuo atsakomybės, kilusios pažeidus nustatytas taisykles.
Įspėjame, kad visa sporto komplekso teritorija, įskaitant pastatų vidų, yra stebima vaizdo kameromis ištisą parą. Objektą saugo UAB „G4S“ saugos tarnyba. <br> </p>
<br>
  <ul>
    <li><a href="http://www.fksuduva.lt/images/stories/taisykles/patekimo_i_stadiona_instrukcija.pdf">Patekimo į stadioną instrukcija</a></li>
    <li><a href="http://www.fksuduva.lt/images/stories/taisykles/ziurovu_elgesio_taisykles_manieze.pdf">Žiūrovų elgesio taisyklės manieže</a></li>
    <li><a href="http://www.fksuduva.lt/images/stories/taisykles/ziurovu_elgesio_taisykles_stadione.pdf">Žiūrovų elgesio taisyklės stadione</a></li>
    <li><a href="http://www.fksuduva.lt/images/stories/taisykles/naudojimosi_futbolo_aikste_manieze_reikalavimai.pdf">Naudojimosi futbolo aikšte manieže reikalavimai</a></li>
  </ul>
 <br><p>
In english:</p>
<ul>
  <li><a href="">Behaviour rules in the stadium</a></li>
  <li><a href="">Internal behaviour rules in the covered sports arena</a></li>
  <li><a href="">Requirements for the use of artificial turf</a></li>
  <li><a href="">Naudojimosi futbolo aikšte manieže reikalavimai</a></li>
</ul>
<br>
<p>
  Visi dokumentai yra PDF formate <br>
Tikimės Jūsų supratingumo ir geranoriško bendradarbiavimo.
VšĮ Marijampolės futbolo klubas
Administracija</p>
    </div>
  </div>
</div>
<br class="clear">
<?php include "footer.php"; ?>
