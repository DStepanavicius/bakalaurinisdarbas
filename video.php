<?php
include "connectDB.php";
$title = "Video medžiaga";
include "header.php"; ?>
<div class="wrapper">
  <div class="main">
    
      <div class="col-12">
        <?php
          if (strpos(htmlspecialchars($_SERVER['REQUEST_URI']), '?a-lyga')) {
            echo '
          <h1>A lygos video medžiaga</h1>
          <div class="col-12">
            <a href="video" class="filter_link">Visi</a>
            <a style="background-color: #0d0d0d;" href="video?a-lyga" class="filter_link">A lyga</a>
            <a href="video?II-lyga" class="filter_link" data-filter="IIlyga">II lyga</a>
          </div>
          <div class="col-12">
          ';
            paginateMedia($conn, "A lyga", "video", 4);
          } elseif (strpos(htmlspecialchars($_SERVER['REQUEST_URI']), '?II-lyga')) {
            echo '
          <h1>II lygos video medžiaga</h1>
          <div class="col-12">
            <a href="video" class="filter_link">Visi</a>
            <a href="video?a-lyga" class="filter_link">A lyga</a>
            <a style="background-color: #0d0d0d;" href="video?II-lyga" class="filter_link" data-filter="IIlyga">II lyga</a>
          </div>
          <div class="col-12">
          ';
            paginateMedia($conn, "II lyga", "video", 4);
          } else {
            echo '
          <h1>Video medžiaga</h1>
          <div class="col-12">
            <a style="background-color: #0d0d0d;" href="video" class="filter_link">Visi</a>
            <a href="video?a-lyga" class="filter_link">A lyga</a>
            <a href="video?II-lyga" class="filter_link" data-filter="IIlyga">II lyga</a>
          </div>
          <div class="col-12">
          ';
            paginateMedia($conn, "visos", "video", 4);
          }
         ?>
    </div>
    <br class="clear">
    </div>
  </div>
</div>
<?php include "footer.php"; ?>
