<?php
include "connectDB.php"; 
$title = "Sakalai";
include "header.php"; ?>
<div class="wrapper">
  <div class="sakalai main">
    <h1>Sūduvos Sakalai</h1>
    <div class="sakalaiCon col-12 test">
      <div class="col-6 col-md-12">
        <h2>„Sūduvos Sakalai“  kviečia tapti komandos dalimi!!!</h2>
        <p>Jei tau svarbu palaikyti mylimą komandą, mėgsti keliauti, trokšti nuotykių bei adrenalino – visada esi laukiamas „Sūduvos sakalų“ tribūnoje. Prisijunk!</p>
        <h3>Sūduvos sakalų kontaktai:</h3>
        <ul class="sakalaiInfo">
          <li><span>Web:</span><br><a href="">www.suduvossakalai.lt</a></li>
          <li><span>Facebook:</span><br><a href="">facebook.com/SuduvosSakalai</a></li>
          <li><span>E-paštas:</span><br><a href="">info@suduvossakalai.lt</a></li>
          <li><span>Skype:</span><p>laurenz1329</p></li>
          <li><span>Telefonas:</span><br><a href="">+37065837794</a></li>
        </ul>
        <h2>Kas tai?</h2>
        <p>„Sūduvos sakalai“ – futbolo mylėtojų grupelė, kuri visomis jėgomis ir išgalėmis palaiko savo mylimą „Sūduvos“ futbolo komandą. Klubo palaikymas ir vardo garsinimas šiems žmonėms – ne tik laisvalaikis, bet gyvenimo būdas.
        „Sūduvos sakalai“ yra nevyriausybinė organizacija, turinti savo nuostatus ir vienijanti apie 30 narių. Mes nuolat dalyvaujame nevyriausybinių organizacijų veikloje Marijampolėje, taip pat vykstame į futbolo fanams skirtus renginius.
         
        „Sūduvos sakalai“ turi savo logotipą, vėliavas ir visą futbolo fanų atributiką, kuri padeda sukurti komandos palaikymą varžybų metu ir tinkamai atstovauti organizacijai. 2012 metais Fanų klubas “Sūduvos sakalai” tapo Marijampolės jaunimo organizacijų tarybos “Apskritasis stalas” nariais.</p>
      </div>
      <div class="col-6 col-md-12">
        <img class="imageTest" src="img/sakalai1.jpg" alt="">
      </div>
      <div class="col-12">
        <h2>Keliaukime kartu!</h2>
        <p>„Sūduvos sakalai“ jau trejus metus nėra praleidę nė vienų savo mylimo komandos rungtynių. Palaikėme „Sūduvą“ Latvijoje, Estijoje, Serbijoje, Švedijoje, ir Austrijoje. Esame aplankę visą Lietuvą, nes kasmet į išvykas kiekviename mieste apslankome po kelis kartus.
        Dažnai kelionėse neapseinama ir be nuotykių, kartais tenka taisyti sugedusią mašiną ar kaip akis išdegus skubėti į rungtynes.
         
        Tačiau visus „sakalus“ veda vienas tikslas – garsiai skanduojant palaikyti savo komandą .
        Ištikimiausi “Sūduvos” fanai su komanda keliauja visur. Jei nori palaikyti savo mylimą komandą išvykoje, bet neturi, su kuo nuvykti ar tiesiog nori prisijungti prie linksmos kompanijos, susisiek su mumis. Kviečiame vykti kartu!!!</p>
      </div>
      <div class="col-12">
        <div class="col-4 col-md-12">
          <img style="max-width: 100%;" src="img/sakalai2.jpg" 
          srcset="
           img/sakalai2_small.jpg 320w
          " 
          sizes="(max-width: 500px) 320vw" alt="">
        </div>
        <div class="col-4 col-md-12">
          <img style="max-width: 100%;" src="img/sakalai3.jpg" 
          srcset="
            img/sakalai3_small.jpg 320w
          " 
          sizes="(max-width: 500px) 320vw" alt="">
        </div>
        <div class="col-4 col-md-12">
          <img style="max-width: 100%;" src="img/sakalai4.jpg" 
          srcset="
            img/sakalai4_small.jpg 320w
          " 
          sizes="(max-width: 500px) 320vw" alt="">
        </div>
      </div>
      <div class="col-12">
        <h2>“Sūduvos sakalų“ istorija</h2>
        <p>Prieš 2008 metų sezoną trys bendraminčiai nusprendė sukurt naują fanų klubą, kurį pavadino „Sūduvos sakalais“. Pagrindinis to iniciatorius ir ilgalaikis vadas buvo Tautwis, kuris ir šiuo metu apsilankęs tribūnoje užveda visus savo sodriu balsu ir profesionaliai mušamo būgno skambesiu.
        Kodėl „sakalai“? Tiesiog buvo galvojama pavadinime naudot kažką lietuviško, o sakalas pirma raide ir raidžių skaičiumi atitiko komandos pavadinimą. Dabar šis pavadinimas yra kaip simbolis, lydintis mus. Nieko ypatingo, tiesiog kaip viena jų rūšis – „sakalas keliautojas“, taip ir „Sūduvos sakalai“ nuolat keliauja su komanda ir ją palaiko, nesvarbu ar pergalė ar pralaimėjimas, pakilimuose ar nuosmukiuose tribūnoje stovės žmonės, mylintys savo komandą. Tai dar jauna palaikymo grupė, 2013 metais švenčianti savo 5 metų sukaktį. Tačiau jau 3 metus nebuvo nei vienų „Sūduvos“ rungtynių, kuriose nebūtų organizuoto „Sūduvos sakalų“ palaikymo.</p>
      </div>
    </div>
  </div>
</div>
<?php include "footer.php"; ?>
