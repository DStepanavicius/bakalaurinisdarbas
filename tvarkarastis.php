<?php
include "connectDB.php"; 
$title = "Tvarkaraštis";
include "header.php"; ?>
<div class="wrapper">
  <div class="main">
    <h1>Tvarkaraštis</h1>
    <div class="col-12">
      <?php getTvarkarastis($conn) ?>
    </div>
    <br class="clear">
  </div>
</div>
<?php include "footer.php"; ?>
