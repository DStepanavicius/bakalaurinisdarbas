<?php
include "connectDB.php";
$title = "Muziejus";
 include "header.php"; ?>
<div class="wrapper">
  <div class="main statTurinys">
    <h1>Muziejus</h1>
    <div class="baltas col-12">
      <h2>Muziejaus įkūrimas</h2>
      <p>2014 metų rudenį, ilgai brandinta idėja įkurti FK „Sūduva“ muziejų pavirto realybe. „Arvi“ futbolo arenos patalpose įkurtame muziejuje savo vietą surado dauguma trofėjų iškovotų „Sūduvos“ futbolininkų.</p>
    <div class="col-12">
      <div class="col-3 col-m-6 col-md-12">
        <img src="img/muziejus1.jpg" 
          srcset="
           img/muziejus1_small.jpg 320w
          " 
          sizes="(max-width: 500px) 320vw" alt="Komados laimėjimai">
      </div>
      <div class="col-3 col-m-6 col-md-12">
        <img src="img/muziejus2.jpg" 
          srcset="
           img/muziejus2_small.jpg 320w
          " 
          sizes="(max-width: 500px) 320vw" alt="Komados atributika">
      </div>
      <div class="col-3 col-m-6 col-md-12">
        <img src="img/muziejus3.jpg" 
          srcset="
           img/muziejus3_small.jpg 320w
          " 
          sizes="(max-width: 500px) 320vw" alt="Komados atributika">
      </div>
      <div class="col-3 col-m-6 col-md-12">
        <img src="img/muziejus4.jpg" 
          srcset="
           img/muziejus4_small.jpg 320w
          " 
          sizes="(max-width: 500px) 320vw" alt="Komandos plakatas">
      </div>
    </div>
    <h2>Apie muziejų</h2>
    <p>Apsilankę naujai įkurtame komandos Sūduva muziejuje galėsite pamatyti daugybę įvairaus tipo laimėtų trofėjų bei kitų panašių u komanda susijusių elemetų.
       Visi trofėjai eksponuojami specialiai jiems pastatytose vitrinose kartu su klubo istoriniais momentais ir dovanomis gautomis kovojant Europos turnyruose. Pakeliui į muziejų futbolo gerbėjams suteikiama galimybė prisiminti pačią futbolo Marijampolės krašte pradžią ir jo raidą iki šių dienų „Sūduvos“ futbolo klubo, kuri pateikiama nuotraukose chronologiškai.</p>
    </div>
  </div>
</div>
<br class="clear">
<?php include "footer.php"; ?>
