<?php
include "connectDB.php";
$title = "Bilietai";
include "header.php"; ?>

<div class="wrapper">
  <div class="bilietai main">
    <h1>Bilietai</h1>
    <div class="sakalaiCon col-12">
      <h2>2016 metų futbolo sezonas</h2>
      <p>Bilietų bei abonementų kainos Marijampolės “Sūduvos” rungtynėms “Arvi” futbolo arenoje 2016 metais: <br>Daugiau informacijos: <br>(8~343) 72 325 <br>(8~ 343) 33 152
      </p>
      <div class="col-4 col-m-6 col-md-12">
        <h3>Stadiono kasoje</h3>
        <table>
          <tr>
            <td>Standartinis</td>
            <td>3.00 &euro;</td>
          </tr>
          <tr>
            <td>Studentams, seniorams, moksliaivimas, žmonėms su negalia</td>
            <td>2.00 &euro;</td>
          </tr>
          <tr>
            <td>Specialus pasiūlymas draugų grupei "5 už 4 kainą"</td>
            <td>12.00&euro;</td>
          </tr>

        </table>
        <p class="under">Specialus pasiūlymas draugų grupei "5 už 4 kainą"</p>
        <div class="bilInfo">
          <p>Stadiono kasa darbą pradeda rungtynių dieną, likus 2 val. iki rugtybių pradžios</p>
        </div>
      </div>
      <div class="col-4 col-m-6 col-md-12">
        <h3>Internetu ir Maximoje</h3>
        <table>
          <tr>
            <td>Standartinis</td>
            <td>3.00 &euro;</td>
          </tr>
          <tr>
            <td>Studentams, seniorams, moksliaivimas, žmonėms su negalia</td>
            <td>2.00 &euro;</td>
          </tr>
          <tr>
            <td>Specialus pasiūlymas draugų grupei "5 už 4 kainą"</td>
            <td>12.00&euro;</td>
          </tr>

        </table>
        <p class="under">Specialus pasiūlymas draugų grupei "5 už 4 kainą"</p>
        <div class="bilInfo">
          <p>Galima pirkti internetu tinklalapyje bilietupasaulis.lt arba prekybos centruose "Maxima" (V.Kudirkos g. 3 ir Bažnyčios g. 38 informacijos skyriuose).</p>
        </div>
      </div>
      <div class="col-4 col-m-6 col-md-12">
        <h3>Abonimentas</h3>
        <table>
          <tr>
            <td>Standartinis</td>
            <td>3.00 &euro;</td>
          </tr>
          <tr>
            <td>Studentams, seniorams, moksliaivimas, žmonėms su negalia</td>
            <td>2.00 &euro;</td>
          </tr>
          <tr>
            <td>Specialus pasiūlymas draugų grupei "5 už 4 kainą"</td>
            <td>12.00&euro;</td>
          </tr>

        </table>
        <p class="under">Kodėl verta pirkti abonimentą?</p>
        <div class="bilInfo">
          <ul>
            <li>Pastovi sėdima vieta visose sezono rungtynėse.</li>
            <li>Nereikia gaišti laiko perkant bilietą stadiono kasoje.</li>
            <li>Visos 18 FK "Sūduva" rungtynės stadione.</li>
            <li>Pirkdami abonimenta sutaupote pinigų.</li>
          </ul>
        </div>
        <div>
          <p>Įsigyti abonimentą galima stadiono kasoje.</p>
        </div>
      </div>
      </div>
    <br class="clear">
  </div>
</div>

<?php include "footer.php"; ?>
