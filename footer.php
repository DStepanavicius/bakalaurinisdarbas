<!--     Footer      -->
<br class="clear">
<footer>
  <div class="footer-menu">
    <h1>Meniu</h1>
    <br>
    <div class="co-t">
      <h2><a href="pagrindinis">Pagrindinis</a></h2>
    </div>
    <div style="position: relative; left: 15px;" class="co-t">
      <h2>Naujienos</h2>
      <ul>
        <li><a href="pagrindine-komanda">Sūduva</a></li>
        <li><a href="suduva-2">Sūduva-2</a></li>
        <li><a href="archyvas">Archyvas</a></li>
      </ul>
    </div>
    <div style="position: relative; left: 15px;" class="co-t">
      <h2>Klubas</h2>
      <ul>
        <li><a href="istorija">Istorija</a></li>
        <li><a href="pasiekimai">Pasiekimai</a></li>
        <li><a href="bilietai">Bilietai</a></li>
        <li><a href="rėmėjai">Rėmėjai</a></li>
      </ul>
    </div>
    <div class="co-t">
      <h2>Komanda</h2>
      <ul>
        <li><a href="pagrindine">Pagrindinė</a></li>
      </ul>
    </div>
    <div class="clear-mm clear-s co-t">
      <h2>Galerijos</h2>
      <ul>
        <li><a href="video">Video</a></li>
        <li><a href="Nuotraukos">Nuotraukos</a></li>
      </ul>
    </div>
    <div class="clear-m co-t">
      <h2>Fanams</h2>
      <ul>
        <li><a href="sakalai">Sūduvos sakalai</a></li>
        <li><a href="nuorodos">Nuorodos</a></li>
      </ul>
    </div>
    <div class="co-t">
      <h2><a href="kontaktai">Kontaktai</a></h2>
    </div>
    <div class="co-t">
      <h2>Sek mus!</h2>
      <ul class="soc">
        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
        <li><a href="#"><i class="fa fa-youtube"></i></a></li>
        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
      </ul>
    </div>
    <br class="clear">
    <hr>
    <p>Kopijuoti, platinti, skelbti FK "Sūduva" tinklapio informaciją ir fotografijas be klubo raštiško sutikimo draudžiama ©2013</p>
  </div>
</footer>
</body>
</html>
