<!--
Header.
-->
<?php
  include "connectDB.php";
  $title = "Pagrindinis";
  include "header.php";
 ?>
<!--
Trys pagrindinės naujienos, kuriose yra atvaizduojamos naujausi įrašai.
functions.php -> 99
style.css     -> 473
index.php     -> 10
-->
<?php getHeaderNews($conn) ?>
<div class="main wrapper">
    <div class="row">
        <!--
        Alygos ir II lygos turnyrinės lentelės
        functions.php -> 28
        style.css     -> 389
        index.php     -> 19
        -->
        <section id="lentele" class="col-4 col-m-12">
            <h2>Turnyrinė lentelė</h2>
            <button id="aMygtukas" type="button" name="button">A lyga</button>
            <button id="2Mygtukas" type="button" name="button">II lyga</button>
            <?php
              getPagrLentele($conn, 'alygalentele');
              getPagrLentele($conn, '2lygalentele');
            ?>
        </section>
        <!--
        Sutrumpinta informcija apie tvarkaraštį. Paskutinės ir sekančios rungtynės.
        functions.php -> 28
        style.css     -> 389
        index.php     -> 19
        -->
        <section id="matchStats"  class="col-8 col-m-12">
            <h2>Rungtynės</h2>
            <section class="col-6 col-md-12">
                <h3>Paskutinės rungtynės</h3>
                <div class="matchWrap">
                    <?php getRezults($conn, "sena"); ?>
                </div>
            </section>
            <section class="col-6 col-md-12">
                <h3>Sekančios rungtynės</h3>
                <div class="matchWrap">
                    <?php getRezults($conn, "nauja"); ?>
                </div>
            </section>
        </section>
    </div>
    <div class="row">
        <!--
        Šoniniame meniu yra patalpinta navigacija į mažesnio svarbu puslapius.
        style.css     -> 389
        index.php     -> 19
        -->
        <aside id="sideMenu" class="col-4 col-m-12">
            <p>Sporto kompleksas</p>
            <ul>
                <li><a href="muziejus">Muziejus</a></li>
                <li><a href="svarbi-informacija">Svarbi informacija</a></li>
                <li><a href="sporto-kompleksas">Apie sporto kompleksą</a></li>
                <li><a href="paslaugos">Paslaugos</a></li>
            </ul>
            <p>Futbolo mokykla</p>
            <ul>
                <li><a href="mokykla-naujienos">Naujienos</a></li>
                <li><a href="http://marijampolesfc.lt/?q=node/6">Futbolo centras</a></li>
                <li><a href="fotogalerija">Fotogalerija</a></li>
            </ul>
            <p>MAFF</p>
            <ul>
                <li><a href="lff-naujienos">LFF III lyga</a></li>
                <li><a href="kititurnyrai">Kiti turnyrai</a></li>
            </ul>
            <p>Sūduvos Sakalai</p>
            <a href="http://suduvossakalai.lt/news.php"><img src="img/remejai/sakalai.png" alt=""></a>
            <p>Bilietai</p>
            <div class="bilietaiNuorodos">
                <div class="col-4 col-s-6">
                    <a href="http://www.bilietai.lt"><img src="img/remejai/bilietai.png" alt=""></a>
                </div>
                <div class="col-4 col-s-6">
                    <a href="#"><img src="img/remejai/arvi.png" alt=""></a>
                </div>
                <div class="col-4 col-s-6">
                    <a href="http://www.mercure.com/gb/europe/index.shtml"><img src="img/remejai/mercure.png" alt=""></a>
                </div>
                <div class="col-4 col-s-6">
                    <a href="http://www.mantinga.lt"><img src="img/remejai/mantinga.png" alt=""></a>
                </div>
                <div class="col-4 col-s-6">
                    <a href="http://www.ecaeurope.com/"><img src="img/remejai/eca.png" alt=""></a>
                </div>
                <div class="col-4 col-s-6">
                    <a href="http://www.visitcyprus.com/index.php/en/"><img src="img/remejai/cyprus.png" alt=""></a>
                </div>
            </div>
        </aside>
        <!--
        Keletos paskutinių naujienų pateikimas pagrindiniame puslapyje.
        functions.php -> 2
        style.css     -> 389
        index.php     -> 103
        -->
        <section class="naujienos col-8 col-m-12">
            <h2>Naujienos</h2>
            <div class="naujWrapper">
                <?php getNaujienosPagrindinis($conn) ?>
            </div>
        </section>
    </div>
</div>
<!--
Pateikiamos paskutinių varžybų įrašų nuorodos.
functions.php -> 2
style.css     -> 389
index.php     -> 103
-->
<div class="row">
    <section class="irasai">
        <h2>A lygos rungtynių įrašai</h2>
        <div class="slider">
            <div class="wrapper">
                <?php getRecords($conn) ?>
            </div>
        </div>
    </section>
</div>
<!--
Rėmėjai. Pateikiama statinė informacja (logotipai) apie komandos rėmėjus
style.css     -> 389
index.php     -> 103
-->
<div class="remejai col-12">
    <div class="row">
        <h2>Generalinis rėmėjas</h2>
        <div class="col-12">
            <img class="a" id="genr" src="img/remejai/sumeda.png" alt="Sumeda">
        </div>
    </div>
    <div class="row">
        <h2>Rėmėjai</h2>
        <div class="col-12">
            <div class="col-4 col-m-4 col-md-3 col-s-6"><img src="img/remejai/sr.png" alt="Statybos ritmas"></div>
            <div class="col-4 col-m-4 col-md-3 col-s-6"><img src="img/remejai/arvi.png" alt="Arvi"></div>
            <div class="col-4 col-m-4 col-md-3 col-s-6"><img src="img/remejai/herbas.png" alt="Marijampolės savivaldybė"></div>
            <br class="clear">
        </div>
    </div>
    <div class="row">
        <h2>Kiti rėmėjai</h2>
        <div class="col-12">
            <div class="col-2  col-md-3 col-s-6"><img src="img/remejai/garfus.png" alt="Garfus"></div>
            <div class="col-2  col-md-3 col-s-6"><img src="img/remejai/mavista.png" alt="Mavista"></div>
            <div class="col-2  col-md-3 col-s-6"><img src="img/remejai/joma.png" alt="Joma"></div>
            <div class="col-2  col-md-3 col-s-6"><img src="img/remejai/kttape.png" alt="Kttape"></div>
            <div class="col-2  col-md-3 col-s-6"><img src="img/remejai/iceco.png" alt="Iceco"></div>
            <div class="col-2  col-md-3 col-s-6"><img src="img/remejai/laikr.png" alt="Piko valanda"></div>
        </div>
    </div>
    <div class="row">
        <h2>Informaciniai rėmėjai</h2>
        <div class="col-12">
            <div class="col-3  col-md-3 col-s-6"><img src="img/remejai/marijampolestv.png" alt="Marijampolės televizija"></div>
            <div class="col-3  col-md-3 col-s-6"><img src="img/remejai/100proc.png" alt="100 procentų"></div>
            <div class="col-3  col-md-3 col-s-6"><img src="img/remejai/kapsai.png" alt="Kapsai"></div>
            <div class="col-3  col-md-3 col-s-6"><img src="img/remejai/suvalkietis.png" alt="Suvalkietis"></div>
        </div>
    </div>
</div>
<br class="clear">
<!--
Poraštė. Poraštės kodč galima rasti atskirame dokumente "footer.php"
-->
<?php include "footer.php" ?>
