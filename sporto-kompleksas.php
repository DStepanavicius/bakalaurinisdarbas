<?php
include "connectDB.php";
$title = "Sporto kompleksas";
include "header.php"; ?>
<div class="wrapper">
  <div class="main statTurinys">
    <h1>Sporto kompleksas</h1>
    <div class="col-12 baltas">
      <h2>Apie sporto kompleksą</h2>
      <p>Viešoji įstaiga MARIJAMPOLĖS FUTBOLO KLUBAS įsteigta 2002 m. Tai ne pelno siekianti organizacija, veikianti sporto ir sveikatingumo organizavimo srityse bei viešai teikianti paslaugas visuomenės nariams.
Pagrindinis VšĮ Marijampolės futbolo klubas veiklos tikslas – propaguoti futbolą kaip efektyvaus poilsio priemonę, ugdyti aukštos kvalifikacijos sportininkus, organizuoti aktyvų laisvalaikio praleidimą, populiarinti sportą, organizuoti sporto renginius, šventes. <br>
Nuo 2006-02-10 iki 2008-08-20 VšĮ Marijampolės futbolo klubas vykdė ES struktūrinių fondų ir Lietuvos Respublikos finansuotą projektą „Marijampolės daugiafunkcinės sporto turizmo viešosios infrastruktūros plėtra“ (toliau-Projektas). Pagal 2006 m. lapkritį pasirašytą paramos sutartį, ES ir LR skirta parama Projekto įgyvendinimui siekė 19 740 235 Lt. VšĮ Marijampolės futbolo klubo dalininkai prie Projekto įgyvendinimo prisidėjo ir savo lėšomis. Iš viso Projekto vykdymui dalininkai skyrė beveik 11 mln. litų, iš kurių pusė buvo skirta Marijampolės miesto savivaldybės, kaip pagrindinės VšĮ Marijampolės futbolo klubo dalininkės įnašas.
VšĮ Marijampolės futbolo klubo vykdyto Projekto tikslas – Marijampolės mieste sukurti naują sporto ir turizmo traukos objektą, skatinti Marijampolės miesto ir regiono žmonių užimtumą, sukurti infrastruktūrą, padėsiančią sumažinti sezoniškumo įtaką futbolo sporto šakos vystymui ir pritrauksiantį į Marijampolės miestą daugiau lankytojų iš Lietuvos ir užsienio.
<br>
Sporto komplekso statybos darbai buvo pradėti 2007 m. gegužę ir baigti 2008 m. rugpjūtį. Projekto vykdymo metu Marijampolės mieste buvo pastatyta daugiafunkcinė sporto ir turizmo bazė, kurią sudaro futbolo stadionas, dengtas sporto maniežas ir futbolo aikštynas. Pastarasis buvo įrengtas dar 2003 m. iš VšĮ Marijampolės futbolo klubo dalininkų lėšų ir užbaigus Projektą prijungtas prie naujai atsiradusių sporto statinių. Futbolo stadiono ir daugiafunkcinio sporto maniežo statybai viso buvo išleista beveik 31 mln. litų.
Visas sporto kompleksas yra išsidėstęs beveik 9 ha sklype, ant Šešupės kranto, strategiškai patogioje miesto vietoje. 2008 m. liepos 6 d. duris atvėręs daugiafunkcinis sporto ir turizmo kompleksas yra pirmas tokio tipo sporto kompleksas Lietuvoje po nepriklausomybės atkūrimo. Objektui suteiktas nacionalinės svarbos objekto statusas.
Sporto kompleksą sudaro 4500 vietų futbolo stadionas su tribūna, 2660 sėdimų vietų daugiafunkcinis sporto maniežas bei kiek ankščiau įrengtas futbolo treniruočių aikštynas, kuriame jau keletą metų treniruojasi ne tik Marijampolės miesto sportininkai, bet ir Lietuvos jaunimo futbolo rinktinė.
Futbolo stadionas pastatytas pagal visus UEFA ir LFF nustatytus reikalavimus. Čia įrengtas aikštės apšvietimas, veikia moderni švieslentė, žiūrovų srautai kontroliuojami elektroninės praėjimo sistemos pagalba. Stadiono pagrindinės tribūnos pirmame aukšte yra įrengti 6 persirengimo kambariai su dušais ir masažo kambariais, dopingo kontrolės punktas, kambariai teisėjams. Antrame pastato aukšte išsidėstę administracinės patalpos su dviem konferencijų salėm.</p>
<div class="col-12">
  <div class="col-3 col-m-6 col-md-12">
    <img src="img/kompleksas1.jpg" 
          srcset="
           img/kompleksas1_small.jpg 320w
          " 
          sizes="(max-width: 500px) 320vw" alt="">
  </div>
  <div class="col-3 col-m-6 col-md-12">
    <img src="img/kompleksas2.jpg" 
          srcset="
           img/kompleksas2_small.jpg 320w
          " 
          sizes="(max-width: 500px) 320vw" alt="">
  </div>
  <div class="col-3 col-m-6 col-md-12">
    <img src="img/kompleksas3.jpg" 
          srcset="
           img/kompleksas3_small.jpg 320w
          " 
          sizes="(max-width: 500px) 320vw" alt="">
  </div>
  <div class="col-3 col-m-6 col-md-12">
    <img src="img/kompleksas4.jpg" 
          srcset="
           img/kompleksas4_small.jpg 320w
          " 
          sizes="(max-width: 500px) 320vw" alt="">
  </div>
</div>
<h2>Maniežas</h2>
<p>
Iš naujai Marijampolės mieste iškilusių sporto statinių didžiausio dėmesio susilaukė analogų Lietuvoje neturintis dengtas sporto maniežas. Toks maniežas kol kas yra vienintelis ir visų Baltijos šalių regione. Išskirtinis pastato dydis ir medinės konstrukcijos patraukia ne vieno miestiečio ir miesto svečio akį. Manieže įrengta pilnų matmenų dirbtinės dangos futbolo aikštė, atitinkanti FIFA 2 žvaigždučių kategoriją, leidžia žaisti futbolą ištisus metus. Manieže taip pat įrengti bėgimo takai lengvaatlečiams, yra persirengimo kambariai su dušais ir masažo kambarėliais. Šaltuoju metų laiku čia vyksta tiek futbolo tiek ir lengvaatlečių treniruotės, organizuojamos draugiškos futbolo varžybos, vyksta vietiniai ir tarptautiniai futbolo turnyrai. Maniežas taip pat puiki vieta masiniams kultūriniams renginiams organizuoti, parodoms bei mugėms rengti ištisus metus.
2008 metais FK „Sūduva“ naująjame stadione sužaidė dvejas tarptautines UEFA taurės varžybas su futbolo komandomis iš Velso ir Austrijos, užbaigė Lietuvos futbolo A lygos čempionatą. 2008 m. stadione savo sėkmę net du kartus išbandė nacionalinė Lietuvos futbolo rinktinė rungtynėse su Moldovos ir Austrijos nacionalinėmis futbolo rinktinėmis.
Džiugu, kad Marijampolės sporto kompleksą gerai įvertino ir jau spėjo pamėgti kaimyninių šalių sportininkai. Futbolo komandos iš Latvijos (FK „Ventspils“, FK „Metalurgs“) ir Lenkijos       (OKS Olstyn, SKS Wigry) 2009 m. pradžioje surengė ne vieną futbolo treniruočių stovyklą sporto manieže. Pasiruošimo 2009 m. futbolo čempionatui metu į maniežą  žaisti draugiškų futbolo varžybų ar rengti futbolo stovyklų atvyko ir Lietuvos futbolo A lygos komandos. Tikimės, kad laikui bėgant sporto kompleksas užsitarnaus dar didesnį populiarumą ir pritrauks ne tik Lietuvos futbolo komandas, bet ir svečius iš užsienio futbolo klubų.</p>
    </div>
  </div>
</div>
<br class="clear">
<?php include "footer.php"; ?>
