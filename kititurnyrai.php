<?php
include "connectDB.php";
$title = "Kitų turnyrų naujienos";
 include "header.php"; ?>
  <div class="wrapper">
    <section style="margin-top: 25px;" class="naujienosKita">
      <h2>Kitų turnyrų naujienos</h2>
      <div class="naujWrapper">
        <?php paginate($conn, "naujienos", "Kiti turnyrai") ?>
      </div>
    </section>
    <br class="clear">
  </div>
<?php include "footer.php"; ?>
