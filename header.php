<?php
  session_start();
  include "functions.php";
?>
<!DOCTYPE html>
<html lang="lt">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="stylesheet" href="style.css">
  <!--[if IE 8]>
 	<link rel="stylesheet" href="styleie.css">
  <script
  src="https://code.jquery.com/jquery-1.12.4.min.js"
  integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
  crossorigin="anonymous"></script>
  <![endif]-->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link href="css/lightbox.min.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700&amp;subset=latin-ext" rel="stylesheet">
  <script src="js/respond.js" async></script>
  <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous" ></script>
  <script src="js/lightbox.min.js" defer></script>
  <script src="scripts.js" defer></script>
  <script type="text/javascript" src="js/modernizr-custom.js" async></script>
  <title><?php echo $title; ?></title>
  <link rel="shortcut icon" type="image/x-icon" href="favicon.png" />
</head>
<body>
  <header>
    <div class="menuWrapper">
    <a href="pagrindinis"><img id="logo" src="img/logo.png"></img></a>
    <!--<a href="pagrindinis"><div id="logo"></div></a>-->
    </div>
    <nav id="nav" class="mob-nav">
      <i id="meniuMygt" class="fa fa-bars" aria-hidden="true"></i>
      <div id="navbar" class="menuWrapper">
        <ul id="main-nav">
          <li><a href="pagrindinis">Pagrindinis</a></li>
          <li><span>Naujienos</span>
            <ul>
              <li><a href="pagrindine">Sūduva</a></li>
              <li><a href="suduva-2">Sūduva-2</a></li>
              <li><a href="archyvas">Archyvas</a></li>
            </ul>
          </li>
          <li><span>Komanda</span>
            <ul>
              <li><a href="pagrindine-komanda">Pagrindinė</a></li>
            </ul>
          </li>
          <li><span>Klubas</span>
            <ul>
              <li><a href="istorija">Istorija</a></li>
              <li><a href="pasiekimai">Pasiekimai</a></li>
              <li><a href="bilietai">Bilietai</a></li>
              <li><a href="remejai">Rėmėjai</a></li>
            </ul>
          </li>
          <li><span>Galerijos</span>
            <ul>
              <li><a href="video">Video</a></li>
              <li><a href="nuotraukos">Nuotraukos</a></li>
            </ul>
          </li>
          <li><span>Fanams</span>
            <ul>
              <li><a href="sakalai">Sūduvos sakalai</a></li>
              <li><a href="nuorodos">Nuorodos</a></li>
            </ul>
          </li>
          <li><a href="kontaktai">Kontaktai</a></li>
        </ul>
      </div>
    </nav>
    <div id="socialMenu">
      <div class="menuWrapper">
        <ul class="clear">
          <li><a href="#"><i class="fa fa-facebook" id="facebook"></i></a></li>
          <li><a href="#"><i class="fa fa-twitter" id="twitter"></i></a></li>
          <li><a href="#"><i class="fa fa-youtube" id="youtube"></i></a></li>
          <li><a href="#"><i class="fa fa-instagram" id="instagram"></i></a></li>
        </ul>
      </div>
    </div>
  </header>
