<?php
include "connectDB.php";
$title = "Duomenų pateikimas";
include "header.php"; ?>
  <div class="main wrapper">
    <section style="margin-top: 25px;" class="naujienos">
      <div id="adminNuorodos">
        <a style="background-color: #0d0d0d" href="admin-panel">Grįžti</a>
        <a style="background-color: #0d0d0d" href="logout">Atsijungti</a>
      </div>
        <br class="clear">
        <?php
        if (isset($_SESSION['id'])) {
          if (strpos($_SERVER['REQUEST_URI'], '?naujienos')) {
            echo '
            <h2>Naujienų pildymas</h2>
            <div class="naujWrapper">
            <form action="'.setNaujienos($conn).'" method="post" enctype="multipart/form-data">
            <table id="alyga">
            <tr>
              <th>Pagrindnis failas</th>
              <th>Papildomi failai</th>
              <th>Antraštė</th>
             
              <th>Skiltis</th>
              <th>Turas</th>
              <th>Rūšis</th>
            </tr>
            <tr>
              <td><input type="file" name="failas"></td>
              <td><input type="file" name="files[]" multiple></td>
              <td><input  name="head" type="text"></td>
              <td><input  name="naujSkiltis" type="text"></td>
              <td><input  name="turas" type="text"></td>
              <td><input  name="rusis" type="text"></td>
            </tr>
            <tr>
              <th colspan="6">Turinys</th>
            </tr>
            <tr>
              <td colspan="6"><textarea style="height:350px;"  name="content" type="text"></textarea></td>
            </tr>
              </table>
              <input class="keitimas" name="prideti" type="submit" value="Pridėti">
            </form>
            <br class="clear">
            <h2>Naujienų keitimas</h2>
            ';
            getNaujienos($conn);

          } elseif (strpos($_SERVER['REQUEST_URI'], '?nuotraukos')) {
            echo '
            <h2>Nuotraukų pildymas</h2>
            <div class="naujWrapper">
            <form action="'.setNuotraukos($conn).'" method="post" enctype="multipart/form-data">
            <table id="alyga">
            <tr>
              <th>Pagrindnis failas</th>
              <th>Papildomi failai</th>
              <th>Skiltis</th>
              <th>Lyga</th>
              <th>Turas</th>
            </tr>
            <tr>
              <td><input type="file" name="failas"></td>
              <td><input type="file" name="files[]" multiple></td>
              <td><input  name="skiltis" type="text"></td>
              <td><input  name="lyga" type="text"></td>
              <td><input  name="turas" type="text"></td>

            </tr>
              </table>
              <input class="keitimas" name="keisti" type="submit" value="Pridėti">
            </form>
            <br class="clear">
            <h2>Nuotraukų keitimas</h2>
            ';
            getNuotraukos($conn);

          } elseif (strpos($_SERVER['REQUEST_URI'], '?video')) {
            echo '
            <h2>Vaizdo įrašų pildymas</h2>
            <div class="naujWrapper">
            <form action="'.setMedia($conn).'" method="post">
            <table id="alyga">
            <tr>
              <th>Failas</th>
              <th>Antraštė</th>
              <th>Lyga</th>
            </tr>
            <tr>
              <td><input  name="failas" type="text"></td>
              <td><input  name="antraste" type="text"></td>
              <td><input  name="lyga" type="text"></td>
            </tr>
              </table>
              <input class="keitimas" name="prideti" type="submit" value="Pridėti">
            </form>
            <br class="clear">
            <h2>Vaizdo įrašų keitimas</h2>
            ';
            getMedia($conn);

          } elseif (strpos($_SERVER['REQUEST_URI'], '?lentele')) {
            echo '
            <h2>A lygos turnyrinės lentelės pildymas</h2>
            <div class="naujWrapper">
            <form action="'.setTurnyrineLentele($conn).'" method="post">
            <table id="alyga">
            <tr>
              <th>Komanda</th>
              <th>Sužaista</th>
              <th>Taškai</th>
              <th>Logo</th>
            </tr>
            <tr>
              <td><input  name="komanda" type="text"></td>
              <td><input  name="suzaista" type="text"></td>
              <td><input  name="taskai" type="text"></td>
              <td><input  name="komlogo" type="text"></td>
            </tr>
              </table>
              <input class="keitimas" name="prideti" type="submit" value="Pridėti">
            </form>
            <br class="clear">
            <h2>A lygos turnyrinės lentelės keitimas</h2>
            ';
            getTurnyrineLentele($conn);
          } elseif (strpos($_SERVER['REQUEST_URI'], '?rungtynes')) {
            echo '
            <h2>Tvarkarasčio pildymas</h2>
            <div class="naujWrapper">
            <form action="'.setTvark8($conn).'" method="post">
            <table id="alyga">
            <tr>
              <th>Nr.</th>
              <th>Komanda 1</th>
              <th>Komanda 2</th>
              <th>Data</th>
              <th>Įvartis 1</th>
              <th>Įvartis 2</th>
              <th>Įrašas</th>
              <th>Logo</th>
              <th>Lyga</th>
            </tr>
            <tr>
              <td><input  name="turas" type="text"></td>
              <td><input  name="komanda1" type="text"></td>
              <td><input  name="komanda2" type="text"></td>
              <td><input  name="data" type="text"></td>
              <td><input  name="ivarciai1" type="text"></td>
              <td><input  name="ivarciai2" type="text"></td>
              <td><input  name="irasas" type="text"></td>
              <td><input  name="logo" type="text"></td>
              <td><input  name="lyga" type="text"></td>
            </tr>
              </table>
              <input class="keitimas" name="prideti" type="submit" value="Pridėti">
            </form>"
            <br class="clear">
            <h2>Tvarkarasčio keitimas</h2>
            ';
            getTvark3($conn);

          } elseif (strpos($_SERVER['REQUEST_URI'], '?komanda')) {
            echo '
            <h2>Nuotraukų pildymas</h2>
            <div class="naujWrapper">
            <form action="'.setKomanda($conn).'" method="post" enctype="multipart/form-data">
            <table id="alyga">
            <tr>
              <th>Pagrindnis failas</th>
              <th>Papildomi failai</th>
              <th>Vardas</th>
              <th>Pozicija</th>
              <th>Ūgis</th>
              <th>Svoris</th>
              <th>Gimimo data</th>
              <th>Tautybė</th>
              <th>Nuo</th>
              <th>Įvarčiai</th>
              <th>Perdavimai</th>
              <th>Išsaugojimai</th>
            </tr>
            <tr>
              <td><input type="file" name="failas"></td>
              <td><input type="file" name="files[]" multiple></td>
              <td><input  name="name" type="text"></td>
              <td><input  name="position" type="text"></td>
              <td style="width: 60px;"><input  name="lenght" type="text"></td>
              <td style="width: 60px;"><input  name="weight" type="text"></td>
              <td><input  name="date" type="text"></td>
              <td><input  name="nation" type="text"></td>
              <td style="width: 60px;"><input  name="since" type="text"></td>
              <td><input  name="goals" type="text"></td>
              <td><input  name="ast" type="text"></td>
              <td><input  name="saves" type="text"></td>
            </tr>
              </table>
              <input class="keitimas" name="prideti" type="submit" value="Pridėti">
            </form>"
            <br class="clear">
            <h2>Nuotraukų keitimas</h2>
            ';
            getKomanda($conn);
          }
        } else {
          header('Location: admin-panel');
        }
        ?>
      </div>
    </section>
    <br class="clear">
  </div>
<?php include "footer.php"; ?>
