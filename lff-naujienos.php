<?php
include "connectDB.php";
$title = "LFF III lygos naujienos";
 include "header.php"; ?>
  <div class="wrapper">
    <section style="margin-top: 25px;" class="naujienosKita">
      <h2>LFF lygos naujienos</h2>
      <div class="naujWrapper">
        <?php paginate($conn, "naujienos", "LFF lyga") ?>
      </div>
    </section>
    <br class="clear">
  </div>
<?php include "footer.php"; ?>
