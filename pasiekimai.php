<?php
include "connectDB.php";
$title = "Pasiekimai";
 include "header.php"; ?>
<div class="wrapper">
  <div class="pasiekimai main">
    <h1>Pasiekimai</h1>
    <div class="sakalaiCon col-12">
      <h2>Bendrai</h2>
      <p>Marijampolės futbolo klubas „Sūduva“ įkurtas 1942 metais. Vėliau klubas daugybę kartų keitė pavadinimą ir tik 1968 metais buvo atkurtas „Sūduvos“vardas. Taigi jau 70 metų ši komanda reprezentuoja ir kovoja dėl Marijampolės, kaip Lietuvos futbolo sostinės, vardo. Europos turnyruose (UEFA) FK „Sūduva“ su pertraukomis rungtyniauja nuo 2002 metų. Šiuo metu „Sūduvos“ futbolo klubas įvardijamas kaip vienas stabiliausių Lietuvoje.</p>
      <h2>Reikšmingos datos „Sūduvos“ istorijoje:</h2>
      <ul>
        <li>1975 metai - FK „Sūduva“ tapo Lietuvos čempionato bronzos medalininkais.</li>
        <li>1976 metai - Lietuvos futbolo federacijos (LFF) taurės finalininkė.</li>
        <li>2001 metai - po dešimties metų pertraukos „Sūduva“ iškovojo teisę žaisti Lietuvos futbolo klubų aukščiausioje (A) lygoje.</li>
        <li>2002 metai - FK „Sūduva“ pirmą kartą dalyvavo UEFA taurės turnyre. LFF taurės finalininkė.</li>
        <li>2005 metai - FK „Sūduva“ po 30 – dešimties metų pertraukos A-lygoje iškovojo bronzos medalius.</li>
        <li>2006  metai - FK „Sūduva“ pirmą kartą klubo istorijoje iškovojo Lietuvos futbolo taurę.</li>
        <li>2007 metai - FK „Sūduva“ iškovojo A lygos čempionato sidabro medalius.</li>
        <li>2009 metai - FK „Sūduva“ iškovojo bronzos medalius A lygoje. Taip pat tapo Baltijos lygos vicečempione, LFF taurės laimėtoja, LFF supertaurės laimėtoja.</li>
        <li>2010 metai - iškovoti A lygos sidabro medaliai.</li>
        <li>2011 metai - iškovoti A lygos bronzos medaliai.</li>
        <li>2012 metai - iškovoti A lygos bronzos medaliai.</li>
      </ul>
      <h2>Europos turnyro rezultatai</h2>
      <table class="desktop">
        <tr>
          <th colspan="4">2002 - 2003</th>
        </tr>
        <tr>
          <th colspan="4">EUFA taurė</th>
        </tr>
        <tr>
          <td>Kvalifikacinis etapas</td>
          <td>„Brann“ (Norvegija)</td>
          <td>3-2 (S) 3-2 (N)</td>
          <td>6-4</td>
        </tr>
        <tr>
          <td>I etapas</td>
          <td>„Celtic“ FC (Škotija)</td>
          <td>1-8 (S) 0-2 (N)</td>
          <td>1-10</td>
        </tr>
        <tr>
          <th colspan="4">2006 - 2007</th>
        </tr>
        <tr>
          <th colspan="4">EUFA taurė</th>
        </tr>
        <tr>
          <td>I etapas</td>
          <td>„Rhyl“ FC (Velsas)</td>
          <td>0-0 (S), 2-1 (N)</td>
          <td>2-1</td>
        </tr>
        <tr>
          <td>II etapas</td>
          <td>„Club Brugge“ (Belgija)</td>
          <td>0-2 (N), 5-2 (S)</td>
          <td>2-7</td>
        </tr>
        <tr>
          <th colspan="4">2007 - 2008</th>
        </tr>
        <tr>
          <th colspan="4">EUFA taurė</th>
        </tr>
        <tr>
          <td>I etapas</td>
          <td>Dungannon „Swifts“ (Š.Airija)</td>
          <td>1-0 (S), 4-0 (N)</td>
          <td>4-1</td>
        </tr>
        <tr>
          <td>II etapas</td>
          <td>SK „Brann“ (Norvegija)</td>
          <td>2-1 (S), 3-4 (N)</td>
          <td>4-6</td>
        </tr>
        <tr>
          <th colspan="4">2009</th>
        </tr>
        <tr>
          <th colspan="4">EUFA taurė</th>
        </tr>
        <tr>
          <td>I etapas</td>
          <td>„TNS“ (Velsas)</td>
          <td>0-0 (S), 2-1 (N)</td>
          <td>2-1</td>
        </tr>
        <tr>
          <td>II etapas</td>
          <td>Salzburgo „Red Bulls“ (Austrija)</td>
          <td>1-4 (N), 0-1 (S)</td>
          <td>2-4</td>
        </tr>
      </table>

      <table class="mobile">
        <tr>
          <th colspan="3">2002 - 2003</th>
        </tr>
        <tr>
          <th colspan="3">EUFA taurė</th>
        </tr>
        <tr>
          <th colspan="3">Kvalifikacinis etapas</th>
        </tr>
        <tr>
          <td>„Brann“ (Norvegija)</td>
          <td>3-2 (S) <br> 3-2 (N)</td>
          <td>6-4</td>
        </tr>
        <tr>
          <th colspan="3">I etapas</th>
        </tr>
        <tr>
          <td>„Celtic“ FC (Škotija)</td>
          <td>1-8 (S) <br> 0-2 (N)</td>
          <td>1-10</td>
        </tr>
        <tr>
          <th colspan="3">2006 - 2007</th>
        </tr>
        <tr>
          <th colspan="3">EUFA taurė</th>
        </tr>
        <tr>
          <th colspan="3">Kvalifikacinis etapas</th>
        </tr>

        <tr>
          <td>„Rhyl“ FC (Velsas)</td>
          <td>0-0 (S), <br> 2-1 (N)</td>
          <td>2-1</td>
        </tr>
        <tr>
          <th colspan="3">I etapas</th>
        </tr>
        <tr>
          <td>„Club Brugge“ (Belgija)</td>
          <td>0-2 (N), <br> 5-2 (S)</td>
          <td>2-7</td>
        </tr>
        <tr>
          <th colspan="3">2007 - 2008</th>
        </tr>
        <tr>
          <th colspan="3">EUFA taurė</th>
        </tr>
        <tr>
          <th colspan="3">I etapas</th>
        </tr>

        <tr>
          <td>Dungannon „Swifts“ (Š.Airija)</td>
          <td>1-0 (S), <br> 4-0 (N)</td>
          <td>4-1</td>
        </tr>
        <tr>
          <th colspan="3">II etapas</th>
        </tr>
        <tr>
          <td>SK „Brann“ (Norvegija)</td>
          <td>2-1 (S), <br> 3-4 (N)</td>
          <td>4-6</td>
        </tr>
        <tr>
          <th colspan="3">2009</th>
        </tr>
        <tr>
          <th colspan="3">EUFA taurė</th>
        </tr>
        <tr>
          <th colspan="3">I etapas</th>
        </tr>

        <tr>
          <td>„TNS“ (Velsas)</td>
          <td>0-0 (S), <br> 2-1 (N)</td>
          <td>2-1</td>
        </tr>
        <tr>
          <th colspan="3">II etapas</th>
        </tr>
        <tr>
          <td>Salzburgo „Red Bulls“ (Austrija)</td>
          <td>1-4 (N), <br> 0-1 (S)</td>
          <td>2-4</td>
        </tr>
      </table>
      <br class="clear">
    </div>
    <br class="clear">
  </div>
</div>
<?php include "footer.php"; ?>
