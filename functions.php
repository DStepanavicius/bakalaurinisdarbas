<?php
/* Naujienų įrašai pagrindiniame puslapyje.
functions.php -> 2
style.css     -> 661
index.php     -> 103
*/
function getNaujienosPagrindinis($conn){
  //Gaunama inforamcija iš duomenų bazės. 5 naujausios
  $sql = "SELECT * FROM naujienos ORDER BY dateNow DESC Limit 5";
  $result = $conn->query($sql);
  //Kiekviena gauta eilutė pereinama per while ciklą
  while ($row = $result -> fetch_assoc()) {
    //Kintamieji
    $data = date( 'Y-m-d H:i', strtotime( $row["dateNow"]));
    $antraste = $row['head'];
    $turinys =  substr($row["content"], 0, 200);
    $paveikselis = $row["image"];
    $numeris = $row["id"];
    if (!$paveikselis) {
        $paveikselis = "img/blank.jpg";
      }
    //Informacija atvaizuojama html kodą. 
    echo '
      <article>
        <div>
        <img src="'.$paveikselis.'" srcset="
        '.preg_replace("/(\.[^.]+)$/", sprintf("%s$1", "_small"), $paveikselis).' 320w,
        '.preg_replace("/(\.[^.]+)$/", sprintf("%s$1", "_medium"), $paveikselis).' 640w,
        '.preg_replace("/(\.[^.]+)$/", sprintf("%s$1", "_big"), $paveikselis).' 1024w
        " alt="'.$antraste.'" sizes="(min-width: 500px) 640vw, (min-width: 1000px) 1024vw">
        </div>
        <div class="naujText">
          <h3>'.$antraste.'</h3>
          <span>'.$data.'</span>
          <p>'.$turinys.'...'.'</p>
        </div>
        <a href="naujienos?id='.$numeris.'" class="naujMygtukas">Plačiau...</a>
        <br class="clear">
      </article>
    ';
  }
}

/* 
Trys pagrindinės naujienos, kuriose yra atvaizduojamos naujausi įrašai. 
functions.php -> 28
style.css     -> 389
index.php     -> 10
*/
function getHeaderNews($conn){
  //Gaunama inforamcija iš duomenų bazės. 3 naujausios
  $sql = "SELECT * FROM naujienos where naujSkiltis = 'Pagrindinė' ORDER BY dateNow DESC LIMIT 3";
  $result = $conn->query($sql);
  //Kintamasis kurio pagalba bus nustatoma, kurioje vietoje informacija bus dedama
  $i = 0;
  //Kiekviena gauta eilutė pereinama per while ciklą
  while ($row = $result -> fetch_assoc()) {
    //Kintamieji
    $antraste = $row['head'];
    $paveikselis = $row["image"];
    $numeris = $row["id"];
    //HTML kodas
    echo '
        <div>
          <a href="naujienos?id='.$numeris.'">
    ';
    //Nustatoma kuris komponentas yra kuriamas
    if ($i === 0) {
      echo '<div class="headerImageLarge">'; 
    } else {
      echo '<div class="headerImageSmall">'; 
    }
    echo '
        <img src="'.$paveikselis.'" srcset="
              '.preg_replace("/(\.[^.]+)$/", sprintf("%s$1", "_small"), $paveikselis).' 320w,
              '.preg_replace("/(\.[^.]+)$/", sprintf("%s$1", "_medium"), $paveikselis).' 640w,
              '.preg_replace("/(\.[^.]+)$/", sprintf("%s$1", "_big"), $paveikselis).' 1024w
              " alt="'.$antraste.'" sizes="(min-width: 500px) 640vw, (min-width: 1000px) 1024vw">
              <div class="overlay">
                <div class="headerImageText">
                  <h1>'.$antraste.'</h1>
                </div>
              </div>
            </div>
          </a>
    ';
    $i++;
  }
}

/* 
Alygos ir II lygos turnyrinės lentelės
functions.php -> 28
style.css     -> 389
index.php     -> 19
*/
function getPagrLentele($conn, $lentele) {
  //Gaunama inforamcija iš duomenų bazės. 3 naujausios
  $sql = "SELECT * FROM $lentele ORDER BY taskai DESC";
  $result = $conn->query($sql);
  //Yra naudojamos dvi skirtingos lentelės todėl reikia skirti 2 skirtingus id
  if ($lentele == "alygalentele") {
    echo '<table id="pagrAlyga">';
  } else {
    echo '<table id="pagr2lyga" style="display: none;">';
  }
  echo'
    <tr>
      <th>Komanda</th>
      <th>Sužaista</th>
      <th>Taškai</th>
    </tr>';
  //Kiekviena gauta eilutė pereinama per while ciklą
  while($row = $result -> fetch_assoc()) {
    echo '
      <tr>
        <td>'.$row['komanda'].'</td>
        <td>'.$row['suzaide'].'</td>
        <td>'.$row['taskai'].'</td>
      </tr>
    ';
  }
  echo '</table>';
}

/* Sutrumpinta informcija apie tvarkaraštį. Paskutinės ir sekančios rungtynės.
functions.php -> 28
style.css     -> 389
index.php     -> 19
*/
function getRezults($conn, $data){
  //Iš duomenų bazės paimamas paskutins ir sekantis įrašai
  $sql = "SELECT * FROM tvarkarastis WHERE data < NOW() ORDER BY data DESC LIMIT 1";
  $result2 = $conn->query($sql);
  $sql = "SELECT * FROM tvarkarastis WHERE data > NOW() ORDER BY data ASC LIMIT 1";
  $result1 = $conn->query($sql);

  //Žiūrima kuri inforamcija yra atvaizduojama paskutinės rungt. ar sekančios
  if ($data == "sena") {
    $row = $result2 -> fetch_assoc();
    $sql = "SELECT * from tvarkarastis, naujienos where naujienos.turas = ".$row["turas"]." and  naujienos.rusis = 'Aprašymas' AND tvarkarastis.turas = '".$row["turas"]."'";
    $result88 = $conn->query($sql);
    $row5 = $result88 -> fetch_assoc();
    $pav = '<a href="nuotraukos?turas='.$row5["turas"].'">Nuotraukos</a>';
    $apr = '<a href="naujienos?id='.$row5["id"].'">Aprašymas</a>';
  } else {
    $row = $result1 -> fetch_assoc();
    $sql = "SELECT * from tvarkarastis, naujienos where naujienos.turas = ".$row["turas"]." and  naujienos.rusis = 'Anonsas' AND tvarkarastis.turas = '".$row["turas"]."'";
    $result88 = $conn->query($sql);
    $row5 = $result88 -> fetch_assoc();
    $pav = '<a href="'.$row5["bilietai"].'">Bilietai</a>';
    $apr = '<a href="naujienos?id='.$row5["id"].'">Anonsas</a>';
  }
  //Kintamieji
  $data = date( 'Y-m-d H:i', strtotime( $row["data"]) );
  $pirmaKomanda = $row["komanda1"];
  $pirmaKomandaIvarciai = $row["ivarciai1"];
  $pirmaKomandaLogo = "";
  $antraKomanda = $row["komanda2"];
  $antraKomandaIvarciai = $row["ivarciai2"];
  $antraKomandaLogo = "";

  //Tikrinama ar logotipas egzistuoja A lygos lentelėje. Jei ne naudojamas clogo lauko logotipas
  $sql3 = "SELECT * FROM alygalentele WHERE komanda = '".$row['komanda1']."'";
  $result5 = $conn->query($sql3);
  if (!$row8 = $result5 -> fetch_assoc()) {
    $pirmaKomandaLogo = $row["cLogo"];
  } else {
    $pirmaKomandaLogo = $row8["logo"];
  }
  $sql4 = "SELECT * FROM alygalentele WHERE komanda = '".$row['komanda2']."'";
  $result6 = $conn->query($sql4);
  if (!$row9 = $result6 -> fetch_assoc()) {
    $antraKomandaLogo = $row["cLogo"];
  } else {
    $antraKomandaLogo = $row9["logo"];
  }
  echo '
    <div class="matchPanel">
      <div class="col-4">
        <img src="'.$pirmaKomandaLogo.'" alt="" />
      </div>
      <div class="rez">'.$pirmaKomandaIvarciai.' : '.$antraKomandaIvarciai.'</div>
      <div class="col-4" style="float: right;">
        <img src="'.$antraKomandaLogo.'" alt="" />
      </div>
      <br class="clear">
    </div>
    <div class="matchInfo">
      <span>'.$pirmaKomanda.' - '.$antraKomanda.'</span>
      <p>'.$data.'</p>
    </div>
    <div class="matchLinks">
      <a href="tvarkarastis">Tvarkaraštis</a>
      '. $apr . $pav .'
    </div>
    <br class="clear">
  ';
}
function getRecords($conn){
  $sql = "SELECT * FROM tvarkarastis,naujienos WHERE tvarkarastis.turas = naujienos.turas and `Irasas` is not null and `Irasas` != '' and `rusis` = 'Aprašymas' ORDER BY data DESC LIMIT 2";
  $result = $conn->query($sql);

  while ($row = $result -> fetch_assoc()) {
    echo '
      <article class="col-6 col-m-12">
        <div class="irasai contentWrapper">
        <div class="col-6 col-md-12 irasaiKorteles">
          <img src="'.$row['image'].'" srcset="
          '.preg_replace("/(\.[^.]+)$/", sprintf("%s$1", "_small"), $row["image"]).' 320w,
          '.preg_replace("/(\.[^.]+)$/", sprintf("%s$1", "_medium"), $row["image"]).' 640w,
          '.preg_replace("/(\.[^.]+)$/", sprintf("%s$1", "_big"), $row["image"]).' 1024w
          " alt="'.$row['head'].'"  sizes="(min-width: 500px) 640vw, (min-width: 1000px) 1024vw">
        </div>
          <div class="col-6 col-md-12">
            <p>'.$row['lyga'].'</p>
            <h3>'.$row['komanda1'].' : '.$row['komanda2'].'</h3>
            <p class="vresult">'.$row['ivarciai1'].' : '.$row['ivarciai2'].'</p>
            <a href="'.$row['Irasas'].'">Žiūrėti</a>

          </div>
          <br class="clear">
        </div>
      </article>
    ';
  }
  mysqli_close($conn);
}


// Individulaių naujienų atvaizdavimas.
function naujienuAtvaizdavimas($conn){
  $id = $_GET["id"];
  $sql = "SELECT * FROM naujienos WHERE id='$id'";
  $result = $conn->query($sql);
  $row = $result -> fetch_assoc();
  $ankstesne = "SELECT * FROM naujienos WHERE dateNow > '".$row["dateNow"]."' ORDER BY dateNow ASC Limit 1";
  $result1 = $conn->query($ankstesne);
  if(!$result1->num_rows == 1) {
    $ankstesne = "SELECT * FROM naujienos WHERE dateNow < '".$row["dateNow"]."' ORDER BY `naujienos`.`dateNow` DESC Limit 1,1";
    $result1 = $conn->query($ankstesne);
  }
  $row1 = $result1 -> fetch_assoc();

  $sekanti = "SELECT * FROM naujienos WHERE dateNow < '".$row["dateNow"]."' ORDER BY `naujienos`.`dateNow` DESC Limit 1";
  $result2 = $conn->query($sekanti);
  if(!$result2->num_rows == 1) {
    $sekanti = "SELECT * FROM naujienos WHERE dateNow > '".$row["dateNow"]."' ORDER BY dateNow ASC Limit 1,1";
    $result2 = $conn->query($sekanti);
  }
  $row2 = $result2 -> fetch_assoc();
  $data = date( 'Y-m-d H:i', strtotime( $row["dateNow"]));
  $paveikselis = $row["image"];
  $paveikselis1 = $row1["image"];
  $paveikselis2 = $row2["image"];

  if (!$paveikselis) {
    $paveikselis = "img/blank.jpg";
  }
  if (!$paveikselis1) {
    $paveikselis1 = "img/blank.jpg";
  }
  if (!$paveikselis2) {
    $paveikselis2 = "img/blank.jpg";
  }
  echo '
  <div class="wrapper">
    <div class="main">
    <div>
    <section class="col-5 col-md-12">
      <img style="width: 100%;" src="'.$paveikselis.'" srcset="
        '.preg_replace("/(\.[^.]+)$/", sprintf("%s$1", "_small"), $paveikselis).' 320w,
        '.preg_replace("/(\.[^.]+)$/", sprintf("%s$1", "_medium"), $paveikselis).' 640w,
        '.preg_replace("/(\.[^.]+)$/", sprintf("%s$1", "_big"), $paveikselis).' 1024w
        " alt="'.$row["head"].'" sizes="(min-width: 500px) 640vw, (min-width: 1000px) 1024vw">
      <div class="mobl">
        <h2 class="kitos">Kitos naujienos</h2>
        <div class="asideNaujienos">
          <div class="col-5 col-m-12 image">
            <img src="'.$paveikselis1.'" srcset="
        '.preg_replace("/(\.[^.]+)$/", sprintf("%s$1", "_small"), $paveikselis1).' 320w,
        '.preg_replace("/(\.[^.]+)$/", sprintf("%s$1", "_medium"), $paveikselis1).' 640w,
        '.preg_replace("/(\.[^.]+)$/", sprintf("%s$1", "_big"), $paveikselis1).' 1024w
        " alt="'.$row1["head"].'" sizes="(min-width: 500px) 640vw, (min-width: 1000px) 1024vw">
          </div>
          <div class="col-7 col-m-12 text">
            <h3>'.$row1["head"].'</h3>
            <p>'.substr($row1["content"], 0, 100) . '...'.'</p>
          </div>
          <a href="naujienos?id='.$row1['id'].'">Daugiau...</a>
        </div>
        <div class="asideNaujienos">
          <div class="col-5 col-m-12 image">
            <img src="'.$paveikselis2.'" srcset="
        '.preg_replace("/(\.[^.]+)$/", sprintf("%s$1", "_small"), $paveikselis2).' 320w,
        '.preg_replace("/(\.[^.]+)$/", sprintf("%s$1", "_medium"), $paveikselis2).' 640w,
        '.preg_replace("/(\.[^.]+)$/", sprintf("%s$1", "_big"), $paveikselis2).' 1024w
        " alt="'.$row2["head"].'" sizes="(min-width: 500px) 640vw, (min-width: 1000px) 1024vw">
          </div>
          <div class="col-7 col-m-12 text">
            <h3>'.$row2["head"].'</h3>
            <p>'.substr($row2["content"], 0, 100) . '...'.'</p>
          </div>
          <a href="naujienos?id='.$row2['id'].'">Daugiau...</a>
        </div>
        </div>
    </section>
    <section class="news col-7 col-md-12">
      <h1>'.$row["head"].'</h1>
      <span>'.$data.'</span>
      <pre>'.$row["content"].'</pre>
    </section>
    </div>
    <br class="clear">
    <div class="dskt">
    <h2 class="kitos">Kitos naujienos</h2>
    <div class="asideNaujienos">
      <div class="col-5 col-m-12 image">
        <img src="'.$row1["image"].'" srcset="
        '.preg_replace("/(\.[^.]+)$/", sprintf("%s$1", "_small"), $row1["image"]).' 320w,
        '.preg_replace("/(\.[^.]+)$/", sprintf("%s$1", "_medium"), $row1["image"]).' 640w,
        '.preg_replace("/(\.[^.]+)$/", sprintf("%s$1", "_big"), $row1["image"]).' 1024w
        " alt="'.$row1["head"].'" sizes="(min-width: 500px) 640vw, (min-width: 1000px) 1024vw">
      </div>
      <div class="col-7 col-m-12 text">
        <h3>'.$row1["head"].'</h3>
        <p>'.substr($row1["content"], 0, 100) . '...'.'</p>
      </div>
      <a href="naujienos?id='.$row1['id'].'">Daugiau...</a>
    </div>
    <div class="asideNaujienos">
      <div class="col-5 col-m-12 image">
        <img src="'.$row2["image"].'" srcset="
        '.preg_replace("/(\.[^.]+)$/", sprintf("%s$1", "_small"), $row2["image"]).' 320w,
        '.preg_replace("/(\.[^.]+)$/", sprintf("%s$1", "_medium"), $row2["image"]).' 640w,
        '.preg_replace("/(\.[^.]+)$/", sprintf("%s$1", "_big"), $row2["image"]).' 1024w
        " alt="'.$row2["head"].'" sizes="(min-width: 500px) 640vw, (min-width: 1000px) 1024vw">
      </div>
      <div class="col-7 col-m-12 text">
        <h3>'.$row2["head"].'</h3>
        <p>'.substr($row2["content"], 0, 100) . '...'.'</p>
      </div>
      <a href="naujienos?id='.$row2['id'].'">Daugiau...</a>
    </div>
    <br class="clear">
    </div>
  </div>  </div>';
}





// Bendras naujienų atvaizdavimas visuose naujienų puslapiuose padalinant įrašus į dalis.
function paginate($conn, $lentele, $skiltis){
  if ($skiltis === "Visos") {
    $sql = "SELECT COUNT(id) FROM $lentele";
  } else {
    $sql = "SELECT COUNT(id) FROM $lentele where naujSkiltis = '$skiltis'";
  }
  $result = $conn->query($sql);
  $row = $result->fetch_array(MYSQLI_NUM);
  $elementuSkaicius = 9;
  $paskutinissk = ceil($row[0]/$elementuSkaicius);
  $x = 1;
  $id = 1;
  if(isset($_GET['id'])){
  	$id = preg_replace('#[^0-9]#', '', $_GET['id']);
  }
  if ($id < $x) {
      $id = $x;
  } else if ($id > $paskutinissk) {
      $id = $paskutinissk;
  }
  $blokas = ($id - $x) * $elementuSkaicius .',' .$elementuSkaicius;
  if ($skiltis === "Visos") {
    $sql = "SELECT * FROM $lentele ORDER BY dateNow DESC limit  $blokas";
  } else {
    $sql = "SELECT * FROM $lentele where naujSkiltis = '$skiltis' ORDER BY dateNow DESC limit $blokas";
  }
  $result = $conn->query($sql);
  $valdymas = '';
  if($paskutinissk != $x){
  	if ($id > $x) {
      $ankstesnis = $id - $x;
  		$valdymas .= '<li><a href="'.htmlspecialchars($_SERVER['PHP_SELF']).'?id='.$ankstesnis.'"><</a></li>';
  		for($i = $id - 3; $i < $id; $i++){
  			if($i > 0){
          if($i <= $id - 3){
            $valdymas .= "...";
            continue;
          }
  		    $valdymas .= '<li><a href="'.htmlspecialchars($_SERVER['PHP_SELF']).'?id='.$i.'">'.$i.'</a></li>';
  			}
  	  }
    }
  	$valdymas .= '<li><a id="selected">'.$id.'</a></li>';
  	for($i = $id + $x; $i <= $paskutinissk; $i++){
  		$valdymas .= '<li><a href="'.htmlspecialchars($_SERVER['PHP_SELF']).'?id='.$i.'">'.$i.'</a></li>';
  		if($i >= $id + 3){
        $valdymas .= "...";
  			break;
  		}
  	}
    if ($id != $paskutinissk) {
        $sekantis = $id + $x;
        $valdymas .= '<li><a href="'.htmlspecialchars($_SERVER['PHP_SELF']).'?id='.$sekantis.'">></a></li> ';
    }
    while ($row = $result -> fetch_assoc()) {
      $paveikselis = $row["image"];
      $data = date( 'Y-m-d H:i', strtotime( $row["dateNow"]));
      if (!$paveikselis) {
        $paveikselis = "img/blank.jpg";
      }
      echo '
        <article>
          <div class="naujImageWrap">
            <img src="'.$paveikselis.'" srcset="
            '.preg_replace("/(\.[^.]+)$/", sprintf("%s$1", "_small"), $paveikselis).' 320w,
            '.preg_replace("/(\.[^.]+)$/", sprintf("%s$1", "_medium"), $paveikselis).' 640w,
            '.preg_replace("/(\.[^.]+)$/", sprintf("%s$1", "_big"), $paveikselis).' 1024w
            " alt="'.$row['head'].'"  sizes="(min-width: 500px) 640vw, (min-width: 1000px) 1024vw">
          </div>
          <div class="naujText">
            <h3>'.$row['head'].'</h3>
            <span>'.$data.'</span>
            <p>'.substr($row["content"], 0, 250).'...</p>
          </div>
          <a href="naujienos?id='.$row['id'].'" class="naujMygtukas">Plačiau...</a>
          <br class="clear">
        </article>
      ';
    }
  }
    echo '
      <div class="pages">
      <ul class="paginate">'.$valdymas.'</ul>
      </div>
    ';
}
function paginateMedia($conn, $filtras, $media, $skaicius){
  if ($filtras == "visos") {
    $sql = "SELECT COUNT(id) FROM $media where skiltis != 'Mokykla'  ORDER BY id DESC";
  } elseif ($filtras == "A lyga" or $filtras == "II lyga") {
    $sql = "SELECT COUNT(id) FROM $media where lyga = '$filtras' and skiltis != 'Mokykla'  ORDER BY id DESC";
  } elseif ($filtras == "Mokykla") {
    $sql = "SELECT * FROM $media where skiltis = 'Mokykla' ORDER BY failas DESC";
  } else {
    $sql = "SELECT COUNT(id) FROM $media where turas = '$filtras' ORDER BY id DESC";
  }
  $result = $conn->query($sql);
  $row = $result->fetch_array(MYSQLI_NUM);

  $elementuSkaicius = $skaicius;
  $paskutinissk = ceil($row[0]/$elementuSkaicius);
  $x = 1;
  $id = 1;
  if(isset($_GET['id'])){
  	$id = preg_replace('#[^0-9]#', '', $_GET['id']);
  }
  if ($id < $x) {
      $id = $x;
  } else if ($id > $paskutinissk) {
      $id = $paskutinissk;
  }
  $blokas = ($id - $x) * $elementuSkaicius .',' .$elementuSkaicius;
  if ($filtras == "visos") {
    $sql = "SELECT * FROM $media where skiltis != 'Mokykla' ORDER BY id DESC limit $blokas";
  } elseif ($filtras == "A lyga" or $filtras == "II lyga") {
    $sql = "SELECT * FROM $media where lyga = '$filtras' and skiltis != 'Mokykla'  ORDER BY id DESC limit $blokas";
  } elseif ($filtras == "Mokykla") {
    $sql = "SELECT * FROM $media where skiltis = 'Mokykla' ORDER BY id DESC limit $blokas";
  } else {
    $sql = "SELECT * FROM $media where turas = '$filtras' ORDER BY id DESC limit $blokas";
  }
  $result = $conn->query($sql);
  $valdymas = '';

  $filtras = str_replace(' ', '-', $filtras);
  $filtras = str_replace('A', 'a', $filtras);
  if($paskutinissk != $x){
  	if ($id > $x) {
      $ankstesnis = $id - $x;
  		$valdymas .= '<li><a href="'.htmlspecialchars($_SERVER['PHP_SELF']).'?id='.$ankstesnis.'"><</a></li>';
  		for($i = $id - 3; $i < $id; $i++){
  			if($i > 0){
          if($i <= $id - 3){
            $valdymas .= "...";
            continue;
          }
  		    $valdymas .= '<li><a href="'.htmlspecialchars($_SERVER['PHP_SELF']).'?id='.$i.'">'.$i.'</a></li>';
  			}
  	  }
    }
  	$valdymas .= '<a id="selected">'.$id.'</a>';
  	for($i = $id + $x; $i <= $paskutinissk; $i++){
  		$valdymas .= '<li><a href="'.htmlspecialchars($_SERVER['PHP_SELF']).'?id='.$i.'">'.$i.'</a></li>';
  		if($i >= $id + 3){
        $valdymas .= "...";
  			break;
  		}
  	}
    if ($id != $paskutinissk) {
          $sekantis = $id + $x;
          $valdymas .= '<li><a href="'.htmlspecialchars($_SERVER['PHP_SELF']).'?id='.$sekantis.'">></a></li> ';
    }
  }
  while ($row = $result -> fetch_assoc()) {
    $lyga = $row['lyga'];
    $lyga = str_replace(' ', '', $lyga);
      if ($media == "video") {
        echo '
          <div class="media '.$lyga.' col-6 col-md-12">
            <div class="video">
              <h2 class="videoAntr">'.$row['antraste'].'</h2>
              <p class="lyga">'.$row['lyga'].'</p>
              <p>'.$row['data'].'</p>
              '.$row['failas'].'
            </div>
          </div>
        ';
      } else {
        echo '
        <div class="media '.$lyga.' col-4 col-m-6 col-md-12 columnFix">
          <div class="video">
            <a href="'.$row['failas'].'" data-lightbox="image-1" data-title="">
              <img src="'.$row['failas'].'" srcset="
              '.preg_replace("/(\.[^.]+)$/", sprintf("%s$1", "_small"), $row['failas']).' 320w,
              '.preg_replace("/(\.[^.]+)$/", sprintf("%s$1", "_medium"), $row['failas']).' 640w,
              '.preg_replace("/(\.[^.]+)$/", sprintf("%s$1", "_big"), $row['failas']).' 1024w
              " sizes="(min-width: 500px) 640vw, (min-width: 1000px) 1024vw">
            </a>
          </div>
        </div>
        ';
      }
  }
  echo '
  <br class="clear">
    <div style="    BACKGROUND-COLOR: #E31B23;" class="pages">
    <ul class="paginate">'.$valdymas.'</ul>
    </div>
  ';
}
function paginatetest($conn, $filtras, $media){
  $sql = "SELECT * FROM $media where turas ='$filtras' ORDER BY id DESC";
  $result = $conn->query($sql);
  $row = $result->fetch_array();
    while ($row = $result -> fetch_assoc()) {
      $lyga = $row['lyga'];
      $lyga = str_replace(' ', '', $lyga);
          echo '
          <div class="media '.$lyga.' col-4 col-m-6 col-md-12">
            <div class="video">
              <a href="'.$row['failas'].'" data-lightbox="image-1" data-title="">
                <img src="'.$row['failas'].'" alt="">
              </a>
            </div>
          </div>
          ';
        }
}

function getBestPlayers($conn){
  $sql = "SELECT vardas,image, MAX(ivarciai) as ivarciai FROM zaidejai GROUP BY id ORDER BY ivarciai DESC LIMIT 1";
  $result = $conn->query($sql);
  while ($row = $result -> fetch_assoc()) {
    echo '
    <div class="col-4 col-m-6 col-md-12">
    <div class="naudingiausi">
      <h2>Įvarčiai</h2>
      <div style="padding: 0;" class="col-6 col-s-12">
        <img src="'.$row['image'].'" srcset="
              '.preg_replace("/(\.[^.]+)$/", sprintf("%s$1", "_small"), $row['image']).' 320w,
              '.preg_replace("/(\.[^.]+)$/", sprintf("%s$1", "_medium"), $row['image']).' 640w,
              '.preg_replace("/(\.[^.]+)$/", sprintf("%s$1", "_big"), $row['image']).' 1024w
              " alt="'.$row['vardas'].'" sizes="(min-width: 500px) 640vw, (min-width: 1000px) 1024vw">
      </div>
      <div class="col-6 col-s-12">
        <h3 style="min-height: 50px;" class="vardas">'.$row['vardas'].'</h3>
        <p class="tekstas">Įmušta įvarčių:</p>
        <p class="numb">'.$row['ivarciai'].'</p>
      </div>
      <br class="clear">
    </div>
    </div>
    ';
  }
  $sql = "SELECT vardas,image, MAX(issaugoti) as issaugoti  FROM zaidejai GROUP BY id ORDER BY issaugoti DESC LIMIT 1";
  $result = $conn->query($sql);
  while ($row = $result -> fetch_assoc()) {
    echo '
    <div class="col-4 col-m-6 col-md-12">
      <div class="naudingiausi">
        <h2>Išsaugoti vartai</h2>
        <div style="padding: 0;" class="col-6 col-s-12">
          <img src="'.$row['image'].'" srcset="
              '.preg_replace("/(\.[^.]+)$/", sprintf("%s$1", "_small"), $row['image']).' 320w,
              '.preg_replace("/(\.[^.]+)$/", sprintf("%s$1", "_medium"), $row['image']).' 640w,
              '.preg_replace("/(\.[^.]+)$/", sprintf("%s$1", "_big"), $row['image']).' 1024w
              " alt="'.$row['vardas'].'" sizes="(min-width: 500px) 640vw, (min-width: 1000px) 1024vw">
        </div>
        <div class="col-6 col-s-12">
          <h3 style="min-height: 50px;" class="vardas">'.$row['vardas'].'</h3>
          <p class="tekstas">Išsaugoti vartai:</p>
          <p class="numb">'.$row['issaugoti'].'</p>
        </div>
        <br class="clear">
      </div>
    </div>
    ';
  }
  $sql = "SELECT vardas,image, MAX(rezultatyvus) as rezultatyvus FROM zaidejai GROUP BY id ORDER BY rezultatyvus DESC LIMIT 1";
  $result = $conn->query($sql);
  while ($row = $result -> fetch_assoc()) {
    echo '
    <div class="col-4 col-m-6 col-md-12">
      <div class="naudingiausi">
        <h2>Perdavimai</h2>
        <div style="padding: 0;" class="col-6 col-s-12">
          <img src="'.$row['image'].'" srcset="
              '.preg_replace("/(\.[^.]+)$/", sprintf("%s$1", "_small"), $row['image']).' 320w,
              '.preg_replace("/(\.[^.]+)$/", sprintf("%s$1", "_medium"), $row['image']).' 640w,
              '.preg_replace("/(\.[^.]+)$/", sprintf("%s$1", "_big"), $row['image']).' 1024w
              " alt="'.$row['vardas'].'" sizes="(min-width: 500px) 640vw, (min-width: 1000px) 1024vw">
        </div>
        <div class="col-6 col-s-12">
          <h3 style="min-height: 50px;" class="vardas">'.$row['vardas'].'</h3>
          <p class="tekstas">Perdavimai:</p>
          <p class="numb">'.$row['rezultatyvus'].'</p>
        </div>
        <br class="clear">
      </div>
    </div>
    ';
  }
}
function getPlayers($conn, $pozicija){
  if ($pozicija == "") {
    $sql = "SELECT * FROM zaidejai where pozicija = 'Treneris' or pozicija = 'Vyr.treneris'";
  } else {
    $sql = "SELECT * FROM zaidejai where pozicija = '$pozicija'";
  }
  $result = $conn->query($sql);
  while ($row = $result -> fetch_assoc()) {
    echo '
    <div class="col-4 col-m-6 col-md-12 test">
      <div class="zaidejaiWrap">
        <div class="col-6 col-s-12 imageWrap">
          <img src="'.$row['image'].'" srcset="
              '.preg_replace("/(\.[^.]+)$/", sprintf("%s$1", "_small"), $row['image']).' 320w,
              '.preg_replace("/(\.[^.]+)$/", sprintf("%s$1", "_medium"), $row['image']).' 640w,
              '.preg_replace("/(\.[^.]+)$/", sprintf("%s$1", "_big"), $row['image']).' 1024w
              " alt="'.$row['vardas'].'" sizes="(min-width: 500px) 640vw, (min-width: 1000px) 1024vw">
        </div>
        <div class="col-6 col-s-12">

          <h3>'.$row['vardas'].'</h3>
          <table>
          ';
          if ($pozicija == "") {
            echo '
            <tr>
              <th>Pareigos:</th>
              <td>'.$row['pozicija'].'</td>
            </tr>
            ';
          } else {
            echo '
              <tr>
                <th>Pozicija:</th>
                <td>'.$row['pozicija'].'</td>
              </tr>
              <tr>
                <th>Ūgis:</th>
                <td>'.$row['ugis'].'</td>
              </tr>
              <tr>
                <th>Svoris:</th>
                <td>'.$row['svoris'].'</td>
              </tr>
              ';
          }
            echo '
            <tr>
              <th>Gimimo data:</th>
              <td>'.$row['gimimoData'].'</td>
            </tr>
            <tr>
              <th>Tautybė:</th>
              <td>'.$row['tautybe'].'</td>
            </tr>
            <tr>
              <th>Sūduvoje nuo:</th>
              <td>'.$row['laikas'].'</td>
            </tr>
          </table>
        </div>
        <br class="clear">
      </div>
    </div>
    ';
  }
}
/*
Administratoriaus darbai su tvarkaraščiu
*/
function getTvark3($conn) {
  $sql = "SELECT * FROM tvarkarastis";
  $result = $conn->query($sql);
  echo "<table id='alyga'>";
  echo"
    <tr>
      <th>Nr.</th>
      <th>Komanda 1</th>
      <th>Komanda 2</th>
      <th>Data</th>
      <th>Įvartis 1</th>
      <th>Įvartis 2</th>
      <th>Įrašas</th>
      <th>Logo</th>
      <th>Lyga</th>
      <th></th>
    </tr>";
  while($row = $result -> fetch_assoc()) {
    $sturas = $row['turas'];
    echo "<tr>";
    echo "<td>" . $row['turas'] . "</td>";
    echo "<td>" . $row['komanda1'] . "</td>";
    echo "<td>" . $row['komanda2'] . "</td>";
    echo "<td><input  name='data' type='text' value='" . $row['data'] . "'> </td>";
    echo "<td>" . $row['ivarciai1']."</td>";
    echo "<td>" . $row['ivarciai2']. "</td>";
    echo "<td><input  name='irasas' type='text' value='" . $row['Irasas']."'> </td>";
    echo "<td><input  name='logo' type='text' value='" . $row['cLogo']. "'> </td>";
    echo "<td>" . $row['lyga']. "</td>";
    echo "<td>
      <form action='keitimas?rungtynes' method='post'>
      <input  name='id' type='hidden' value='" . $row['RungtId'] . "'>
      <input  name='sturas' type='hidden' value='" . $sturas . "'>
      <input  name='turas' type='hidden' value='" . $row['turas'] . "'>
      <input  name='komanda1' type='hidden' value='" . $row['komanda1'] . "'>
      <input  name='komanda2' type='hidden' value='" . $row['komanda2'] . "'>
      <input  name='data' type='hidden' value='" . $row['data'] . "'>
      <input  name='ivarciai1' type='hidden' value='" . $row['ivarciai1']."'>
      <input  name='ivarciai2' type='hidden' value='" . $row['ivarciai2']. "'>
      <input  name='irasas' type='hidden' value='" . $row['Irasas']."'>
      <input  name='logo' type='hidden' value='" . $row['cLogo']. "'>
      <input  name='lyga' type='hidden' value='" . $row['lyga']. "'>
      <input type='submit' value='Keisti'>
      </form>
    </td>";
    echo "</tr>";

  }
  echo "</table>";
}

function tvarkymas($conn) {
  if (isset($_POST['keisti'])){
  
    $sturas = $_POST['sturas'];
    $turas = $_POST['turas'];
    $data = $_POST['data'];
    $komanda1 = $_POST['komanda1'];
    $komanda2 = $_POST['komanda2'];
    $ivarciai1 = $_POST['ivarciai1'];
    $ivarciai2 = $_POST['ivarciai2'];
    $irasas = $_POST['irasas'];
    $logo = $_POST['logo'];
    $lyga = $_POST['lyga'];
    $sql =  "UPDATE tvarkarastis SET turas ='$turas', data='$data', komanda1='$komanda1', komanda2='$komanda2', ivarciai1='$ivarciai1', ivarciai2='$ivarciai2', Irasas='$irasas', cLogo='$logo', lyga='$lyga' WHERE  turas = '$sturas'";

    $result = $conn->query($sql);
    header("Location: admin?rungtynes");
  }
  if (isset($_POST['trinti'])){
    $id = $_POST['id'];
    $sql =  "DELETE FROM tvarkarastis WHERE RungtId ='$id'";
    $result = $conn->query($sql);
     header("Location: admin?rungtynes");
   
  }
}
function setTvark8($conn) {
  if (isset($_POST['prideti'])){
    $turas = $_POST['turas'];
    $data = $_POST['data'];
    $komanda1 = $_POST['komanda1'];
    $komanda2 = $_POST['komanda2'];
    $ivarciai1 = $_POST['ivarciai1'];
    $ivarciai2 = $_POST['ivarciai2'];
    $irasas = $_POST['irasas'];
    $logo = $_POST['logo'];
    $lyga = $_POST['lyga'];
    $sql =  "INSERT INTO tvarkarastis (turas, data, komanda1, komanda2, ivarciai1, ivarciai2, Irasas, clogo, lyga) VALUES ('$turas', '$data', '$komanda1', '$komanda2', '$ivarciai1', '$ivarciai2', '$irasas', '$logo', '$lyga')";
    $result = $conn->query($sql);
  }
}
/*
Administratoriaus darbai su turnyrine lentele
*/
function getTurnyrineLentele($conn) {
  $sql = "SELECT * FROM alygalentele";
  $result = $conn->query($sql);
  echo "
    <table id='alyga'>
    <tr>
      <th>Komanda</th>
      <th>Sužaista</th>
      <th>Taškai</th>
      <th>Logo</th>
      <th></th>
    </tr>";
    while($row = $result -> fetch_assoc()) {
      echo "
    <tr>
      <td>".$row['komanda']."</td>
      <td>".$row['suzaide']."</td>
      <td>".$row['taskai']."</td>
      <td>".$row['logo']."</td>
      <td>
        <form action='keitimas?lentele' method='post'>
          <input  name='skomanda' type='hidden' value='".$row['komanda']."'>
          <input  name='komanda' type='hidden' value='".$row['komanda']."'>
          <input  name='suzaista' type='hidden' value='".$row['suzaide']."'>
          <input  name='taskai' type='hidden' value='".$row['taskai']."'>
          <input  name='logo' type='hidden' value='".$row['logo']."'>
          <input type='submit' value='Keisti'>
        </form>
      </td>
    </tr>
    ";
  }
  echo "
      </table>
    ";
}
function setTurnyrineLentele($conn) {
  if (isset($_POST['prideti'])){
    $taskai = $_POST['taskai'];
    $suzaista = $_POST['suzaista'];
    $komanda = $_POST['komanda'];
    $komlogo = $_POST['komlogo'];
    $sql =  "INSERT INTO alygalentele (komanda, suzaide, taskai, logo) VALUES ('$komanda', '$suzaista', '$taskai', '$logo')";
    $result = $conn->query($sql);
  }
 
}
function tvarkymasLenteles($conn) {
  if (isset($_POST['keisti'])){
    $skomanda = $_POST['skomanda'];
    $taskai = $_POST['taskai'];
    $suzaista = $_POST['suzaista'];
    $komanda = $_POST['komanda'];
    $komlogo = $_POST['logo'];
    $sql =  "UPDATE alygalentele SET komanda ='$komanda', taskai='$taskai', suzaide='$suzaista', logo='$komlogo' WHERE  komanda = '$skomanda'";

    $result = $conn->query($sql);
    header("Location: admin?lentele");
  }
  if (isset($_POST['trinti'])){
    $skomanda = $_POST['skomanda'];
    $sql =  "DELETE FROM alygalentele WHERE komanda ='$skomanda'";
    $result = $conn->query($sql);
    header("Location: admin?lentele");
  }
}
/*
Administratoriaus darbai su komanda
*/
function getKomanda($conn) {
  $sql = "SELECT * FROM zaidejai";
  $result = $conn->query($sql);
  echo "
    <table id='alyga'>
    <tr>
      <th>Nuotrauka</th>
      <th>Vardas</th>
      <th>Pozicija</th>
      <th>Ūgis</th>
      <th>Svoris</th>
      <th>Gimimo data</th>
      <th>Tautybė</th>
      <th>Nuo</th>
      <th>Įvarčiai</th>
      <th>Perdavimai</th>
      <th>Išsaugojimai</th>
      <th></th>
    </tr>";
    while($row = $result -> fetch_assoc()) {
      echo "
    <tr>
      <td>".$row['image']."</td>
      <td>".$row['vardas']."</td>
      <td>".$row['pozicija']."</td>
      <td>".$row['ugis']."</td>
      <td>".$row['svoris']."</td>
      <td>".$row['gimimoData']."</td>
      <td>".$row['tautybe']."</td>
      <td>".$row['laikas']."</td>
      <td>".$row['ivarciai']."</td>
      <td>".$row['rezultatyvus']."</td>
      <td>".$row['issaugoti']."</td>
      <td>
        <form action='keitimas?komanda' method='post'>
          <input  name='id' type='hidden' value='".$row['id']."'>
          <input  name='image' type='hidden' value='".$row['image']."'>
          <input  name='name' type='hidden' value='".$row['vardas']."'>
          <input  name='position' type='hidden' value='".$row['pozicija']."'>
          <input  name='lenght' type='hidden' value='".$row['ugis']."'>
          <input  name='weight' type='hidden' value='".$row['svoris']."'>
          <input  name='date' type='hidden' value='".$row['gimimoData']."'>
          <input  name='nation' type='hidden' value='".$row['tautybe']."'>
          <input  name='since' type='hidden' value='".$row['laikas']."'>
          <input  name='goals' type='hidden' value='".$row['ivarciai']."'>
          <input  name='ast' type='hidden' value='".$row['rezultatyvus']."'>
          <input  name='saves' type='hidden' value='".$row['issaugoti']."'>
          <input type='submit' value='Keisti'>
        </form>
      </td>
    </tr>
    ";
  }
    echo "
      </table>
    ";
}
function setKomanda($conn) {
  if (isset($_POST['prideti'])){
    $target_dir = "img/";
    $target_file = $target_dir . basename($_FILES["failas"]["name"]);
    $uploadOk = 1;
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
    if (file_exists($target_file)) {
        echo "<p class='error'>Lygiai tokio pat pavadinimo paveikslėlis jau egzistuoja. Įkelkite kitą pav.</p>";
        $uploadOk = 0;
    } else {
        if (move_uploaded_file($_FILES["failas"]["tmp_name"], $target_file)) {
            foreach($_FILES['files']['tmp_name'] as $key => $tmp_name ){
              $file_name = $_FILES['files']['name'][$key];
              $target_filee = $target_dir . basename($file_name);
              move_uploaded_file($_FILES['files']['tmp_name'][$key], $target_filee);
            }
            $name = $_POST['name'];
            $position = $_POST['position'];
            $lenght = $_POST['lenght'];
            $weight = $_POST['weight'];
            $date = $_POST['date'];
            $nation = $_POST['nation'];
            $since = $_POST['since'];
            $goals = $_POST['goals'];
            $ast = $_POST['ast'];
            $saves = $_POST['saves'];
            $sql =  "INSERT INTO zaidejai (image, vardas, pozicija, ugis, svoris, gimimoData, tautybe, laikas, ivarciai, rezultatyvus, issaugoti)
            VALUES ('$target_file', '$name', '$position', '$lenght', '$weight', '$date', '$nation', '$since', '$goals', '$ast', '$saves')";
            $result = $conn->query($sql);
        }
    }
  }
}
function tvarkymasKomanda($conn) {
  if (isset($_POST['keisti'])){
    $id = $_POST['id'];
    $image = $_POST['image'];
    $name = $_POST['name'];
    $position = $_POST['position'];
    $lenght = $_POST['lenght'];
    $weight = $_POST['weight'];
    $date = $_POST['date'];
    $nation = $_POST['nation'];
    $since = $_POST['since'];
    $goals = $_POST['goals'];
    $ast = $_POST['ast'];
    $saves = $_POST['saves'];

    $sql =  "UPDATE zaidejai SET image ='$image', vardas='$name', pozicija='$position', ugis='$lenght', svoris='$weight', gimimoData='$date', tautybe='$nation', laikas='$date', ivarciai='$goals', rezultatyvus='$ast', issaugoti='$saves' WHERE  id = '$id'";

    $result = $conn->query($sql);
    header("Location: admin?komanda");
  }
  if (isset($_POST['trinti'])){
    $id = $_POST['id'];
    $failas = $_POST['image'];
    unlink($failas);
    unlink(preg_replace("/(\.[^.]+)$/", sprintf("%s$1", "_small"), $failas));
    unlink(preg_replace("/(\.[^.]+)$/", sprintf("%s$1", "_medium"), $failas));
    unlink(preg_replace("/(\.[^.]+)$/", sprintf("%s$1", "_big"), $failas));
    $sql =  "DELETE FROM zaidejai WHERE id = '$id'";
    $result = $conn->query($sql);
    header("Location: admin?komanda");
  }
}

/*
Administratoriaus darbai su naujienomis
*/
function getNaujienos($conn) {
  $sql = "SELECT * FROM naujienos ORDER BY dateNow DESC";
  $result = $conn->query($sql);
  echo "
    <table id='alyga'>
    <tr>
      <th>Nuotrauka</th>
      <th>Antraštė</th>
      <th>Skiltis</th>
      <th>Turas</th>
      <th>Rūšis</th>
      <th></th>
    </tr>";
    while($row = $result -> fetch_assoc()) {
      echo "
    <tr>
      <td class='aa'><textarea disabled>".$row['image']."</textarea></td>
      <td class='aa'><textarea disabled>".$row['head']."</textarea></td>
      <td class='aaa'>".$row['naujSkiltis']."</td>
      <td class='aa'>".$row['turas']."</td>
      <td class='aa'>".$row['rusis']."</td>
      <td class='aaa'>
      </td>
      <td class='aaa'>
        <form action='keitimas?naujienos' method='post'>
          <input  name='id' type='hidden' value='".$row['id']."'>
          <input  name='image' type='hidden' value='".$row['image']."'>
          <input  name='head' type='hidden' value='".$row['head']."'>
          <input  name='content' type='hidden' value='".$row['content']."'>
          <input  name='naujSkiltis' type='hidden' value='".$row['naujSkiltis']."'>
          <input  name='turas' type='hidden' value='".$row['turas']."'>
          <input  name='rusis' type='hidden' value='".$row['rusis']."'>
          <input type='submit' value='Keisti'>
        </form>
      </td>
    </tr>
    <tr>
      <td colspan='7'><textarea class='content' disabled>".$row['content']."</textarea></td>
    </tr>
    ";
  }
    echo "
      </table>
    ";
}
function setNaujienos($conn) {
  if (isset($_POST['prideti'])){

    $target_dir = "img/";
    $target_file = $target_dir . basename($_FILES["failas"]["name"]);
    
    $uploadOk = 1;
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
    
    if (file_exists($target_file)) {
        echo "<p class='error'>Lygiai tokio pat pavadinimo paveikslėlis jau egzistuoja. Įkelkite kitą pav.</p>";
        $uploadOk = 0;
    } else {
        if (move_uploaded_file($_FILES["failas"]["tmp_name"], $target_file)) {
            foreach($_FILES['files']['tmp_name'] as $key => $tmp_name ){
              $file_name = $_FILES['files']['name'][$key];
              $target_filee = $target_dir . basename($file_name);
              move_uploaded_file($_FILES['files']['tmp_name'][$key], $target_filee);
            }
            $name = $_POST['head'];
            $position = $_POST['content'];
            $lenght = $_POST['naujSkiltis'];
            $weight = $_POST['turas'];
            $date = $_POST['rusis'];
            $sql =  "INSERT INTO naujienos (image, head, content, naujSkiltis, turas, rusis)
            VALUES ('$target_file', '$name', '$position', '$lenght', '$weight', '$date')";
            $result = $conn->query($sql);
        }
    }
  }
}
function tvarkymasNaujienos($conn) {
  if (isset($_POST['keisti'])){
    $id = $_POST['id'];
    $image = $_POST['image'];
    $head = $_POST['head'];
    $content = $_POST['content'];
    $naujSkiltis = $_POST['naujSkiltis'];
    $turas = $_POST['turas'];
    $rusis = $_POST['rusis'];
    $sql =  "UPDATE naujienos SET image ='$image', head='$head', content='$content', naujSkiltis='$naujSkiltis', turas='$turas', rusis='$rusis' WHERE  id = '$id'";
    $result = $conn->query($sql);
    header("Location: admin?naujienos");
  }
  if (isset($_POST['trinti'])){
    $id = $_POST['id'];
    $failas = $_POST['image'];
    unlink($failas);
    unlink(preg_replace("/(\.[^.]+)$/", sprintf("%s$1", "_small"), $failas));
    unlink(preg_replace("/(\.[^.]+)$/", sprintf("%s$1", "_medium"), $failas));
    unlink(preg_replace("/(\.[^.]+)$/", sprintf("%s$1", "_big"), $failas));
    $sql =  "DELETE FROM naujienos WHERE id ='$id'";
    $result = $conn->query($sql);
    header("Location: admin?naujienos");
  }
}


/*
Administratoriaus darbai su video medžiaga
*/
function getMedia($conn) {
  $sql = "SELECT * FROM video";
  $result = $conn->query($sql);
  echo "
    <table id='alyga'>
    <tr>
      <th>Failas</th>
      <th>Antraštė</th>
      <th>Lyga</th>
      <th></th>
    </tr>";
    while($row = $result -> fetch_assoc()) {
      echo "
    <tr>
      <td>".$row['failas']."</td>
      <td>".$row['antraste']."</td>
      <td>".$row['lyga']."</td>
      <td>
        <form action='keitimas?video' method='post'>
          <input  name='id' type='hidden' value='".$row['id']."'>
          <input  name='failas' type='hidden' value='".$row['failas']."'>
          <input  name='antraste' type='hidden' value='".$row['antraste']."'>
          <input  name='lyga' type='hidden' value='".$row['lyga']."'>
          <input type='submit' value='Keisti'>
        </form>
      </td>
    </tr>
    ";
  }
    echo "
      </table>
    ";
}
function setMedia($conn) {
  if (isset($_POST['prideti'])){
    $failas = $_POST['failas'];
    $antraste = $_POST['antraste'];
    $lyga = $_POST['lyga'];

    $sql =  "INSERT INTO video (failas, antraste, lyga)
    VALUES ('$failas', '$antraste', '$lyga')";
    $result = $conn->query($sql);
  }
}
function tvarkymasMedia($conn) {
  if (isset($_POST['keisti'])){
    $id = $_POST['id'];
    $failas = $_POST['failas'];
    $antraste = $_POST['antraste'];
    $lyga = $_POST['lyga'];
    $sql =  "UPDATE video SET failas ='$failas', antraste='$antraste', lyga='$lyga' WHERE  id = '$id'";
    $result = $conn->query($sql);
    header("Location: admin?video");
  }
  if (isset($_POST['trinti'])){
    $id = $_POST['id'];
    $failas = $_POST['failas'];
    $sql =  "DELETE FROM video WHERE id ='$id'";
    $result = $conn->query($sql);
    header("Location: admin?video");
  }
}

/*
Administratoriaus darbai su nuotraukų failais
admin.php ->
keitimas.php ->
*/
function getNuotraukos($conn) {
  $sql = "SELECT * FROM paveikslėliai";
  $result = $conn->query($sql);
  echo "
    <table id='alyga'>
    <tr>
      <th>Failas</th>
      <th>Skiltis</th>
      <th>Lyga</th>
      <th>Turas</th>
      <th></th>
      <th></th>
    </tr>";
    while($row = $result -> fetch_assoc()) {
      echo "
    <tr>
      <td>".$row['failas']."</td>
      <td>".$row['skiltis']."</td>
      <td>".$row['lyga']."</td>
      <td>".$row['turas']."</td>
      <td> 
      </td>
      <td>
        <form action='keitimas?nuotraukos' method='post'>
          <input  name='id' type='hidden' value='".$row['id']."'>
          <input  name='failas' type='hidden' value='".$row['failas']."'>
          <input  name='skiltis' type='hidden' value='".$row['skiltis']."'>
          <input  name='lyga' type='hidden' value='".$row['lyga']."'>
          <input  name='turas' type='hidden' value='".$row['turas']."'>
          <input type='submit' value='Keisti'>
        </form>
      </td>
    </tr>
    ";
  }
    echo "
      </table>
    ";
}
function setNuotraukos($conn) {
  if (isset($_POST['keisti'])){
    $target_dir = "img/";
    $target_file = $target_dir . basename($_FILES["failas"]["name"]);
    $uploadOk = 1;
    $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
    
    if (file_exists($target_file)) {
        echo "<p class='error'>Lygiai tokio pat pavadinimo paveikslėlis jau egzistuoja. Įkelkite kitą pav.</p>";
        $uploadOk = 0;
    } else {
        if (move_uploaded_file($_FILES["failas"]["tmp_name"], $target_file)) {
            foreach($_FILES['files']['tmp_name'] as $key => $tmp_name ){
              $file_name = $_FILES['files']['name'][$key];
              $target_filee = $target_dir . basename($file_name);
              move_uploaded_file($_FILES['files']['tmp_name'][$key], $target_filee);
            }
            $skiltis = $_POST['skiltis'];
            $lyga = $_POST['lyga'];
            $turas = $_POST['turas'];
            $sql =  "INSERT INTO paveikslėliai (failas, skiltis, lyga, turas)
            VALUES ('$target_file', '$skiltis', '$lyga', '$turas')";
            $result = $conn->query($sql);
        }
    }
  }
}
function tvarkymasNuotraukos($conn) {
  if (isset($_POST['keisti'])){
    $id = $_POST['id'];
    $failas = $_POST['failas'];
    $skiltis = $_POST['skiltis'];
    $lyga = $_POST['lyga'];
    $turas = $_POST['turas'];
    $sql =  "UPDATE paveikslėliai SET failas ='$failas', skiltis='$skiltis', lyga='$lyga', turas='$turas' WHERE  id = '$id'";
    $result = $conn->query($sql);
    header("Location: admin?nuotraukos");
  }
  if (isset($_POST['trinti'])){
    $id = $_POST['id'];
    $failas = $_POST['failas'];
    unlink($failas);
    unlink(preg_replace("/(\.[^.]+)$/", sprintf("%s$1", "_small"), $failas));
    unlink(preg_replace("/(\.[^.]+)$/", sprintf("%s$1", "_medium"), $failas));
    unlink(preg_replace("/(\.[^.]+)$/", sprintf("%s$1", "_big"), $failas));
    $sql =  "DELETE FROM paveikslėliai WHERE id ='$id'";
    $result = $conn->query($sql);
    header("Location: admin?nuotraukos");
  }
}
// function prisijungimas($conn){
//   if (isset($_POST['adminsubmit'])){
//     $uid = mysqli_real_escape_string($conn, $_POST['uid']);
//     $pwd = mysqli_real_escape_string($conn, $_POST['pwd']);;
//     $stmt = $conn->prepare("SELECT * FROM vartotojai WHERE vardas=? and slaptazodis=?");
//     $stmt->bind_param("ss", $username, $password);
//     $username = $uid;
//     $password = $pwd;
//     $stmt->execute();
//     $resultd = $stmt->get_result();
//     $rowNum = $resultd->num_rows;
//     if ($rowd = $resultd -> fetch_assoc()) {
//       $_SESSION['id'] = $rowd['adminid'];
//       header('Location: admin-panel');
//     }
//   }
//   mysqli_close($conn);
// }
function prisijungimas($conn){
  if (isset($_POST['adminsubmit'])){

    $uid = stripslashes($_POST['uid']);
    $pwd = stripslashes($_POST['pwd']);
    $uid = mysqli_real_escape_string($conn, $_POST['uid']);
    $pwd = mysqli_real_escape_string($conn, $_POST['pwd']);;
    
    $sql = "SELECT * FROM vartotojai WHERE vardas = '$uid' AND slaptazodis = '$pwd' ";
    $result = $conn->query($sql);
    if ($rowd = $result -> fetch_assoc()) {
      $_SESSION['id'] = $rowd['adminid'];
      header('Location: admin-panel');
    }
  }
  mysqli_close($conn);
}
function getTvarkarastis($conn) {
  $sql = "SELECT * FROM tvarkarastis ORDER BY data ";
  $result = $conn->query($sql);
  echo "<table id='alyga'>";
  echo"
    <tr>
      <th class='mobileT'>Turas</th>
      <th>Komandos</th>
      <th>Data</th>
      <th>Rezultatas</th>
      <th class='mobileT'>Lyga</th>
    </tr>";
  while($row = $result -> fetch_assoc()) {
    $data = date( 'Y-m-d H:i', strtotime( $row["data"]));
    $sturas = $row['turas'];
    echo "<tr>";
    echo "<td class='mobileT'>" . $row['turas'] . "</td>";
    echo "<td>" . $row['komanda1'] . " : " . $row['komanda2'] . "</td>";
    echo "<td>" . $data . " </td>";
    echo "<td>" . $row['ivarciai1'].":". $row['ivarciai2']."</td>";
    echo "<td class='mobileT'>" . $row['lyga']. "</td>";
    echo "</tr>";
  }
  echo "</table>";
}