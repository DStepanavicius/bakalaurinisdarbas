<?php
include "connectDB.php";
$title = "Futbolo mokyklos naujienos";
 include "header.php"; ?>
  <div class="wrapper">
    <section style="margin-top: 25px;" class="naujienosKita">
      <h2>Futbolo mokyklos naujienos</h2>
      <div class="naujWrapper">
        <?php paginate($conn, "naujienos", "Mokykla") ?>
      </div>
    </section>
    <br class="clear">
  </div>
<?php include "footer.php"; ?>
