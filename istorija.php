<?php
include "connectDB.php";
$title = "Komandos istorija";
 include "header.php"; ?>

<div class="wrapper">
  <div class="istorija main">
    <h1>Istorija</h1>
    <div class="sakalaiCon col-12 test">
      <h2>Įsikūrimas, pavadinimų gausa ir pirmieji titulai</h2>
      <p>
        Pirmąsias draugiškas rungtynes „Sūduva“ sužaidė su praeityje garsia Kybartų „Sveikatos“ komanda. Dalyvavimą Suvalkijos apygardos pirmenybėse „Sūduvos“ komanda pradėjo pergalėmis prieš Kazlų Rūdos „Ažuolą“, „Orijos“ komandas. Deja, pergalės, galėjusios atvesti į šalies čempionatą, pasiekti nepavyko – buvo pralaimėta Kybartų „Sveikatos“ komandai.
        1943 metais „Sūduva“ Suvalkijos apygardoje dar kartą liko antra į priekį praleisdama Kybartų „Sveikatą“.
        Po šių metų Marijampolės reprezentacinė komanda ne kartą keitė pavadinimus – istorijoje kalbama apie Marijampolės „Žalgirį“, „Spartaką“, „CF“, KTŪ „Žalgirį“, „Mokslą“, „Šešupę“ ir kt. Kartas nuo karto sugrįždavo ir „Sūduvos“ pavadinimas.
        1962 metais Marijampolės „Šešupė“ pirmą kartą istorijoje prasimušė į Lietuvos A klasės čempionato finalą, kuriame nusileido Kauno „Limos“ komandai.
        1970 metais Kapsuko „Sūduva“ žaidė nesėkmingais ir iškrito iš aukščiausios lygos. Tiesa, jau kitais metais kapsukiečiai laimėjo A klasės Nemuno pogrupį ir iškovojo teisę žaisti su geriausiomis komandomis.
        1975 metais „Sūduva“ rungtyniavo sėkmingai. „Nemuno“ pogrupyje užėmę antrąją vietą, galutinėje čempionato rikiuotėje „sūduviečiai“ liko treti ir pasipuošė bronzos medaliais. Tais pačiais metais Kapsuko futbolininkai prasimušė ir į taurės finalą, kuriame buvo priversti pripažinti Tauragės komandos pranašumą.
        Tokį pat pasiekimą „Sūduva“ pakartojo po metų. Tuomet kapsukiečiai finale pralaimėjo Kauno futbolininkams.
      </p>
      <h2>Sensacingas debiutas Europoje ir įsitvirtinimas Lietuvos futbolo žemėlapyje</h2>
      <p>
        Po poros sėkmingų sezonų „Sūduva“ daugiau kaip dešimtmečiui pasitraukė į futbolo pasaulio šešėlį. Sugrįžtį į aukščiausiąją lygą Kapsuko komanda sugebėjo 1988 metais.
        1990 metais Marijampolės „Sūduva“ rungtyniavo naujai įkurtoje Baltijos lygoje. Tiesa, tiek šie, tiek kiti metai marijampoliečiams buvo nesėkmingi ir komanda prarado teisę žaisti aukščiausioje lygoje.
        Nuo 1991 iki 2001 „Sūduva“ blaškėsi tarp I ir III Lietuvos lygų. Tiesa 1993-94 metų sezone Mariajmpolės komanda buvo sugrįžusi į aukščiausiąją komandą „Žydriaus“ pavadinimu, tačiau čia užsibuvo tik vieną sezoną.
        Lužis „Sūduvos“ istorijoje įvyko 2001 metais, kai Marijampolės komanda užėmė antrąją vietą I lygoje bei iškovojo teisę žaisti LFF A lygoje.
        2001 metais „Sūduva“ ne tik sėkmingai pasirodė I lygos varžybose, tačiau ir sugebėjo išlikti kovoje dėl LFF taurės. 2002 metų sezone taurės pusfinalyje „Sūduva“ eliminavo Šiaulių „Sakalo“ komandą ir prasimušė į finalą, kuriame laukė akistata su šalies čempionu FBK „Kaunu“.
        Finale „Sūduva“ patyrė pralaimėjimą ir jau trečią kartą klubo istorijoje liko per žingsnį nuo taurės laimėjimo. Vis dėlto svarbiausia buvo ne  tai – patekimas į turnyro finalą lėmė tai, kad vos pirmą sezoną A lygoje žaiždianti „Sūduva“ prasimušė į UEFA taurės varžybas.
        Po UEFA taurės burtų paaiškėjęs varžovas susidomėjimą Marijampolės komanda pakėlė iki dar neregėtų aukštumų – visi laukė susitikimo su vienu garsiausių Norvegijos futbolo klubų Bergeno „Brann“.
      </p>
      <h2>„Tamsieji“ dešimtmečiai bei šuolis į UEFA spindesį</h2>
      <p>
        Pirmasis sezonas sugrįžus į aukščiausią šalies futbolo lyga susiklostė vidutiniškai. 2002 metais „Sūduva“ A lygoje užėmė 6 vietą tarp 9 komandų ir įsitvirtino čempionate.
   
        Nors pirmame sezone aukščiausiose šalies futbolo lygoje „Sūduva“ buvo lygos vidutiniokė, tačiau debiutas UEFA taurės turnyre buvo įspudingas. Pirmame etape „Sūduva“ tiek išvykoje, tiek namuose (rungtynės buvo žaidžiamos Kaune) iškovojo pergales vienodu rezultatu 3:2 prieš daugkartinius Norvegijos čempionus Bergeno „Brann“ futbolininkus. Antrame atrankos etape laukė garsi komanda iš Škotijos – Glazgo „Celtic“. Pirmosios rungtynės vyko Glazge, kur „Celtic“ futbolininkai įrodė savo pranašumą ir viską nusprendė jau pirmose rungtynėse iškovodami pergalę rezultatu 8:1. Antrosios rungtynės Kaune jau buvo tik formalumas, tose rungtynėse „Sūduva“ nusileido „Celtic“ rezultatu 0:2.
         
        2002 metų sezoną galime laikyti gana sėkmingu „Sūduvai“: komanda įsitvirtino A lygoje, iškovojo istorinę pergalę UEFA taurės turnyre prieš Bergeno „Brann“, žaidė LFF taurės turnyro finale. Šiame sezone įspudingai rungtyniavęs Tomas Radzinevičius buvo pripažintas geriausiu Lietuvoje rungtyniaujančiu futbolininku.
         
        2003 bei 2004 metų sezonai buvo kiek nykesni, nei debiutinis A lygoje. „Sūduva“ 2003 metais ir vėl užėmė 6 vietą A lygos čempionate, tuo tarpu 2004 metais, nukrito dar vienu laipteliu žemiau ir liko 7 vietoje. Lietuvos futbolo federacijos taurės turnyre tiek 2003 metais, tiek 2004 metais „Sūduva“ pasitraukė jau aštunfinalyje.
         
        Istorinis sezonas „Sūduvos“ klubui buvo 2005 metais, kuomet po 30 metų pertraukos komanda iškovojo A lygos bronzos medalius, tuo pačiu iškovodama teisę rungtyniauti kitų metų UEFA taurės atrankos turnyre. Jau po metų „Sūduva“ iškovojo Lietuvos futbolo federacijos taurę, kuomet finale po Dariaus Maciulevičiaus greito įvarčio iškovojo pergalę rezultatu 1:0 įveikė Panevėžio „Ekraną“. UEFA taurės turnyro pirmąjame atrankos etape „Sūduva“ susitiko su ekipa iš Velso – „Rhyl“. Railio mieste, Velse, rungtynės baigėsi „Sūduvai“ palankiomis lygiosiomis – 0:0, o po dviejų savaičių Marijampolėje „Sūduva“ iškovojo sunkią pergalę rezultatu 2:1, tai reiškė, kad marijampoliečiai pateko į antrąjį atrankos etapą. Burtai lėmė, kad antrame etape „Sūduvos“ laukė viena garsiausių Belgijos komandų – „Club Brugge“. Pirmosios rungtynės vyko Kaune. Komanda iš Belgijos iškovojo pergalę rezultatu 2:0, nors pačioje rungtynių pabaigoje rezultatą sušvelninti galėjo „Sūduvos“ brazilas Otavio Braga, tačiau „Club Brugge“ komandą išgelbėjo vartų skersinis. Rungtynės Belgijoje buvo taip pat sunkios, jose pergalę vėl iškovojo garsioji belgų ekipa. Rungtynės baigėsi „Sūduvos“ nesėkme rezultatu 5:2.
      </p>
      <p>
        Jau 2007 metais „Sūduva“ tapo A lygos vicečempionais, nusileidę tik FBK „Kauno“ futbolininkams. UEFA taurės turnyrė vėl buvo pasiektas antrasis atrankos etapas. Pirmame etape „Sūduva“ bendru rezultatu 4:1 įveikė „Dungannon Swifts“ iš Šiaurės Airijos (Šiaurės Airijoje pralaimėtai 1:0, Marijampolėje iškovota pergalė 4:0). Antrame etape laukė marijampoliečiams jau pažystamas varžovas – Bergeno „Brann“. Pirmos rungtynės ir vėl vyko Norvegijoje, kur „Brann“ iškovojo pergalę minimaliu rezultatu 2:1. Šį kartą atsakomosios rungtynės vyko Marijampolėje. Po atkaklios kovos „Sūduva“ pripažino varžovų pranašumą rezultatu 4:3. Šis rezultatas reiškė, kad „Sūduva“ baigė savo pasirodymą Europos turnyruose. Tais pačiais metais marijampoliečiai žaidė ir Lietuvos futbolo federacijos supertaurės finale prieš FBK „Kauno“ komandą. Rungtynes 1:0 laimėjo kauniečiai.
   
        2008 metais „Sūduvai“ nepavyko iškovoti prizinės vietos A lygoje. Nors taškų buvo surinkta tiek pat, kiek turėjo trečioje vietoje likusi Vilniaus „Vėtra“, „Sūduva“ liko ketvirtoje turnyrinės lentelės pozicijoje dėl prastesnio tarpusavio rungtynių santykio. UEFA taurės rungtynėse jau naująjame stadione (vėliau pavadintame „Arvi“ futbolo arena) „Sūduva“ susitiko su „The New Saints“ iš Velso. Namuose marijampoliečiai iškovojo sunkią pergalę rezultatu 1:0, Velse tokiu pačiu rezultatu pergalę ir vėl iškovojo „Sūduvos“ futbolininkai. Antrame atrankos etape marijampoliečiai susigrūmė su klubu iš Austrijos – Zalcburgo „Red Bull“. Namuose „Sūduva“, nors ir pirmavo, tačiau pralaimėjo Austrijos klubui rezultatu 4:1. Išvykoje „Sūduva“ iškovojo netikėtą pergalę 1:0, tačiau į kitą etapą žengė „Red Bull“.
      </p>
      <p>
        Sugrįžimo į Lietuvos stipriausiųjų ekipų trejetą ilgai laukti nereikėjo. Jau 2009 metais „Sūduva“ užėmė trečiąją vietą A lygos čempionate, taip pat „Sūduva“ 5 kartą žaidė Lietuvos futbolo federacijos taurės finale. Šį kartą marijampoliečių varžovais buvo Tauragės „Tauro“ futbolininkai. „Sūduvos“ futbolininkai iškovojo šį trofėjų įveikdami varžovus 1:0. Praėjus vos kelioms savaitėms po triumfo taurės turnyro finale, „Sūduva“ iškovojo ir Supertaurę, kuomet finale tik po 11 metrų baudinių įveikė Panevėžio „Ekrano“ futbolininkus. UEFA taurės turnyre „Sūduva“ susitiko su „Randers“ ekipa iš Danijos, deja Marijampolėje „sūduviečiai“ pralaimėjo 1:0, o Danijoje rungtynėms pasibaigus 1:1 „Sūduva“ savo pasirodymą 2009 metų Europos turnyruose baigė. Nepaisant to, šis sezonas buvo vienas geriausių per klubo istoriją: per sezoną iškovota LFF taurė, Supertaurė bei iškovoti A lygos bronzos medaliai.
   
        Po metų „Sūduva“ pakilo dar per vieną laiptelį į viršų ir dar kartą tapo A lygos vice-čempionais. Marijampolėje vyko Baltijos Taurės finalas, kuriame žaidė ir „Sūduva“. Kita finalininkė buvo viena stipriausių kaimyninės Latvijos komandų – „Ventspils“. Po dramatiškų rungtynių ir 11 metrų baudinių serijos „Sūduva“ nusileido svečiams iš Latvijos ir tapo Baltijos Taurės vicečempione. UEFA Eurpos lygos atrankoje „Sūduvai“ teko susigrumti su Vienos „Rapid“ ekipa. Austrijos ekipa abejose rungtynėse įrodė savo pranašumą (Marijampolėje iškovojo pergalę 0:2, Vienoje 4:2).
         
        2011 metais „Sūduva“ jau trečius metus iš eilės lipo ant A lygos prizininkų pakylos. Marijampoliečiai rikiavosi už Panevėžio „Ekrano“ ir Vilniaus „Žalgirio“ nugarų bei užėmė trečiąją vietą. Europos lygos atrankoje „Sūduva“ nusileido „IF Elfsborg“ ekipai iš Švedijos. Marijampolėje rungtynės baigėsi lygiosiomis 1:1, tačiau išvykoje „IF Elfsborg“ iškovojo įtikinamą pergalę rezultatu 3:0. Po metų, 2012 metais, „Sūduva“ dar kartą iškovojo trečiąją vietą Lietuvos futbolo aukščiausioje lygoje. Tai buvo jau septintas kartas, kai „Sūduva“ tapo Lietuvos čempionato prizininke. Europos lygos atrankos varžybų pirmame etape „Sūduva“ eliminavo Rygos „Daugava“ ekipą. Nors Marijampolėje „Sūduva“ pralaimėjo rezultatu 0:1, tačiau Rygoje sugebėjo laimėti 3:2 ir dėka didesnio kiekio įmuštų įvarčių išvykoje, į kitą etapą pateko „Sūduva“. Antrame etape „Sūduva“ susitiko su „Vojvodina“ iš Serbijos. Pirmosios rungtynės Serbijoje pasibaigė „Sūduvai“ palankiu rezultatu 1:1, tačiau Marijampolėje „Vojvodina“ įveikė „sūduviečius“ 0:4.
         
        Keturių iš eilės prizinių vietų A lygos čempionate, „Sūduvos“ serija nutrūko 2013 metais. Marijampoliečiai liko ketvirtoje vietoje, nors intriga išsilaikė iki paskutinių čempionato minučių. Nors „Sūduva“ paskutiniame ture įveikė būsimus A lygos čempionus „Žalgirio“ futbolininkus, tačiau paskutinę teisėjo pridėto laiko minutę Panevėžio „Ekranas“ įmušė įvartį į Tauragės „Tauro“ vartus, kuris reiškė, kad marijampoliečiams šiais metais nepavyks žengti ant čempionato prizininkų pakylos. Europos lygos atrankoje „sūduviečiai“ iškrito jau pirmąjame atrankos etape. Šį kartą teko susidurti su Makedonijos atstovais – „Turnovo“. Rungtynės Marijampolėje ir Makedonijoje baigėsi tokiu pat rezultatu 2:2. Tai reiškė, kad antrose rungtynėse Makedonijoje buvo žaidžiamas pratęsimas, per kurį nei viena komanda nepasižymėjo. Rungtynių nugalėtojas paaiškėjo tik po 11 metrų baudinių serijos, kuri „Sūduvai“ susiklostė nesėkmingai.
      </p>
    </div>
    <br class="clear">
  </div>
</div>

<?php include "footer.php"; ?>
