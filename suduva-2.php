<?php
include "connectDB.php";
$title = "Sūduva 2";
include "header.php"; ?>
  <div class="wrapper">
    <section class="naujienos naujienosKita">
      <h2>Sūduva-2 naujienos</h2>
      <div class="naujWrapper">
        <?php paginate($conn, "naujienos", "Sūduva 2") ?>
      </div>
    </section>
    <br class="clear">
  </div>
<?php include "footer.php"; ?>
