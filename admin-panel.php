<?php
include "connectDB.php";
$title = "Administratoriaus langas";
include "header.php"; ?>
  <div class="main wrapper">
    <section style="margin-top: 25px;" class="naujienos">
        <?php
        if (isset($_SESSION['id'])) {
          echo '
          <h2>Administratoriaus langas</h2>
          <div style="height: 500px;" class="col-12 naujWrapper">
          <p>Pasirinkite kurios kategorijos duomenis norite keisti:</p>
          <br class="clear">
          <div id="adminNuorodos">
            <a href="admin?naujienos">Naujienos</a>
            <a href="admin?nuotraukos">Nuotraukos</a>
            <a href="admin?video">Video failai</a>
            <a href="admin?lentele">Turnyrinė lentelė</a>
            <a href="admin?rungtynes">Tvarkaraštis</a>
            <a href="admin?komanda">Komanda</a>
            <a style="background-color: #0d0d0d" href="logout">Atsijungti</a>
          </div>
          ';
        } else {
         echo '
         <h2>Prisijungimas</h2>
         <div class="col-12 naujWrapper">
        <div class="login">
          <form class="" action="'.prisijungimas($conn).'" method="post">
            <input type="text" name="uid" value="Vardas"><br>
            <input type="text" name="pwd" value="Slaptažodis"><br>
            <input type="submit" name="adminsubmit" value="Prisijungti"><br>

          </form>
        </div>
        ';
      }
         ?>
      </div>
    </section>
    <br class="clear">
  </div>
<?php include "footer.php"; ?>
