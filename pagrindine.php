<?php
include "connectDB.php";
$title = "Pagrindinė komanda";
include "header.php"; ?>
  <div class="wrapper">
    <section class="naujienosKita">
      <h2>Pagrindinės komandos naujienos</h2>
      <div class="naujWrapper">
        <?php paginate($conn, "naujienos", "Pagrindinė") ?>
      </div>
    </section>
    <br class="clear">
  </div>
<?php include "footer.php"; ?>
