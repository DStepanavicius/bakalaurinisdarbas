<?php
include "connectDB.php";
$title = "Nuorodos";
 include "header.php"; ?>
<div class="wrapper">
  <div class="nuorodos main">
    <h1>Naudingos nuorodos</h1>
    <div class="sakalaiCon col-12 test">
      <div>
        <h2>futbolinis.lt</h2>
        <p>Lietuvos futbolo enciklopedija Futbolinis.lt - savanorių nuo 2009 m. kuriamas projektas, kurio tikslas - patikimai ir kokybiškai surinkti kuo daugiau informacijos apie Lietuvos futbolą, bei šią informaciją pateikti visiems lietuviško futbolo mėgėjams.Čia rasite informaciją apie įvairių laikmečių Lietuvos futbolą, taip pat labai įvairią statistiką. Deja, savanorių resursai gana riboti, todėl ne visą čia pateikiamą informaciją rasite lengvai, bet svetainė pastoviai kinta ir ilgainiui naršymas po enciklopedijos puslapius darysis vis mažiau klaidus.</p>
        <a href="http://futbolinis.lt/">Apsilankyti</a>
        <br class="clear">
      </div>
      <div>
        <h2>futbolo.tv</h2>
        <p>Futbolo.TV yra internetinė futbolo televizija, jau penktus metus tiesiogiai transliuojanti lietuvišką futbolą. Mūsų komanda Jums transliuoja A lygos, LFF Pirmos lygos, Elitinės jaunių lygos čempionatų rungtynes, Lietuvos nacionalinės rinktinės ir jaunimo rinktinių rungtynes, rengia karščiausius reportažus iš Lietuvos ir užsienio, keliauja kartu su Lietuvos nacionaline futbolo rinktine, supažindina su garsiausiais ir perspektyviausiais šalies futbolininkais. Futbolo.TV Lietuvos futbolo gerbėjams operatyviai pateikia aktualiausią ir svarbiausią informaciją, statistiką, komandų sudėtis, rungtynių apžvalgas, įvarčius ir komentarus.</p>
        <a href="http://futbolo.tv/">Apsilankyti</a>
        <br class="clear">
      </div>
      <div>
        <h2>suduvossakalai.lt</h2>
        <p>„Sūduvos sakalai“ – futbolo mylėtojų grupelė, kuri visomis jėgomis ir išgalėmis palaiko savo mylimą „Sūduvos“ futbolo komandą. Klubo palaikymas ir vardo garsinimas šiems žmonėms – ne tik laisvalaikis, bet gyvenimo būdas. „Sūduvos sakalai“ yra nevyriausybinė organizacija, turinti savo nuostatus ir vienijanti apie 30 narių. Mes nuolat dalyvaujame nevyriausybinių organizacijų veikloje Marijampolėje, taip pat vykstame į futbolo fanams skirtus renginius.   „Sūduvos sakalai“ turi savo logotipą, vėliavas ir visą futbolo fanų atributiką, kuri padeda sukurti komandos palaikymą varžybų metu ir tinkamai atstovauti organizacijai. 2012 metais Fanų klubas “Sūduvos sakalai” tapo Marijampolės jaunimo organizacijų tarybos “Apskritasis stalas” nariais.</p>
        <a href="http://suduvossakalai.lt/">Apsilankyti</a>
        <br class="clear">
      </div>
      <div>
        <h2>suduva.wordpress.com</h2>
        <p>Blogas aprašantis Lietuvos futbolo įpatumus bei nuolat informauojantis apie naujausius futbolo komandos Sūduva įvykius. Kiekvieną savaitę galima surasti daug įdomių ir informuojančių straipsnių apie futbolą. Norintys diskutuoti ne tik apie Sūduvą, bet ir kitus futbolo klubus yra kviečiami apsilankyti šiame bloge</p>
        <a href="http://suduva.wordpress.com">Apsilankyti</a>
        <br class="clear">
      </div>
    </div>
    <br class="clear">
  </div>
</div>
<?php include "footer.php"; ?>
